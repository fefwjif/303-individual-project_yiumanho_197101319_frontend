//
//  MyFriendsListVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON

class MyFriendsListVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource  {
    
    var myFriendsModule = [MyUserModule]()
    
    @IBOutlet weak var myTableView: UITableView!
    
    var myKey:[String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.addSubview(myRefreshControl)
        
        myKey = ["friendsArr":self.myCurrentUser.friends, "uuid":[self.myCurrentUser.uuid], "hobby":self.myCurrentUser.hobby]
        
        self.myGetFriends(url: self.myNetwork.myFriends, key: myKey!, method: .post, okMsg: "myGetFriends ok", errorMsg: "myGetFriends error", myReload: {
            self.myTableView.reloadData()
            self.myRefreshControl.endRefreshing()
        })
        
    }
    
    override func myRefreshTableAction() {
        self.myGetFriends(url: self.myNetwork.myFriends, key: myKey!, method: .post, okMsg: "myGetFriends ok", errorMsg: "myGetFriends error", myReload: {
            self.myTableView.reloadData()
            self.myRefreshControl.endRefreshing()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myFriendsModule.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell", for: indexPath) as! FriendCell
        
        cell.userIcon.layer.borderWidth = 2
        cell.userIcon.layer.masksToBounds = false
        cell.userIcon.layer.borderColor = UIColor.systemBlue.cgColor
        cell.userIcon.layer.cornerRadius = cell.userIcon.frame.height/2
        cell.userIcon.clipsToBounds = true
        
//        if self.myFriendsModule[indexPath.row].icon != "null"{
            
            if myFriendsModule[indexPath.row].isLoadedIcon {
                cell.userIcon.image = self.myFriendsModule[indexPath.row].iconContent
            }else{
                Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(self.myFriendsModule[indexPath.row].uuid).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                    if error != nil {
                        self.myPrint(msg: error!.localizedDescription)
                        self.myFriendsModule[indexPath.row].iconContent = UIImage(named: "User")!
                        self.myFriendsModule[indexPath.row].isLoadedIcon = true
                        cell.userIcon.image = self.myFriendsModule[indexPath.row].iconContent
                        return
                    }
                    if let d = data{
                        self.myFriendsModule[indexPath.row].iconContent = UIImage.init(data: d)!
                        self.myFriendsModule[indexPath.row].isLoadedIcon = true
                        cell.userIcon.image = self.myFriendsModule[indexPath.row].iconContent
                    }
                })
            }
            
//        }else {
//            cell.userIcon.image = UIImage(named: "User")!
//        }
        
        cell.userName.text = self.myFriendsModule[indexPath.row].name
        cell.userHobby.text = "Age: \(Int(self.myFriendsModule[indexPath.row].age))"
        cell.userAge.text = self.myFriendsModule[indexPath.row].gender
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.myCurrentUser.selectedUserAge = "\(Int(self.myFriendsModule[indexPath.row].age))"
        self.myCurrentUser.selectedUserUUID = self.myFriendsModule[indexPath.row].uuid
        self.myCurrentUser.selectedUserName = self.myFriendsModule[indexPath.row].name
        self.myCurrentUser.selectedUserHobby = self.myFriendsModule[indexPath.row].hobby
        self.myCurrentUser.selectedUserGender = self.myFriendsModule[indexPath.row].gender
        self.myCurrentUser.selectedUserImage = self.myFriendsModule[indexPath.row].icon
        self.myCurrentUser.selectedUserIntro = self.myFriendsModule[indexPath.row].introduction
        self.myCurrentUser.selectedUserIcon = self.myFriendsModule[indexPath.row].iconContent
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            
            if let c = self.storyboard?.instantiateViewController(identifier: "UserInfoVC") as? UserInfoVC{
                self.navigationController?.pushViewController(c, animated: true)
            }
        }
    }
}

extension MyFriendsListVC {
    
    func myGetFriends(url:String, key:[String:Any], method:HTTPMethod, okMsg:String, errorMsg:String, myReload: @escaping  ()->()){
        
        self.myAFNetworkRequest(okMessage: okMsg, errorMessage: errorMsg, url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
            
            DispatchQueue.main.async {
                self.myPrint(msg: json)
                
                let friendsJsonArr = json["value"]
                var userModule = [MyUserModule]()
                
                guard friendsJsonArr.count != 0 else{
                    self.myFriendsModule = [MyUserModule]()
                    myReload()
                    return
                }
                
                for index in 0...friendsJsonArr.count - 1{
                    userModule.append(MyUserModule.init(uuid: friendsJsonArr[index]["uuid"].stringValue,
                                                        name: friendsJsonArr[index]["name"].stringValue,
                                                        age: friendsJsonArr[index]["age"].doubleValue,
                                                        hobby: friendsJsonArr[index]["gender"].stringValue,
                                                        gender: friendsJsonArr[index]["gender"].stringValue,
                                                        icon: friendsJsonArr[index]["image"].stringValue,
                                                        introduction: friendsJsonArr[index]["introduction"].stringValue,
                                                        friends: friendsJsonArr[index]["friends"].arrayObject as! [String],
                                                        isLoadedIcon: false,
                                                        iconContent: UIImage.init(named: "User")!))
                }
                self.myFriendsModule = userModule
                myReload()
            }
        }
    }
}

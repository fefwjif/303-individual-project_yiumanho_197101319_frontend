//
//  AddBlogVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import Alamofire
import SwiftyJSON
import Speech

class AddBlogVC: MyBaseVC, SFSpeechRecognizerDelegate {
    
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var blogContent: UITextView!
    @IBOutlet weak var blogImage: UIImageView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var myMicBtn: UIButton!
    @IBOutlet weak var contentText: UILabel!
    
    
    let regularFont = UIFont.systemFont(ofSize: 22)
    let boldFont = UIFont.boldSystemFont(ofSize: 22)
    let mySpeechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    
    var myRecognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    var myRecognitionTask: SFSpeechRecognitionTask?
    let myAudioEngine = AVAudioEngine()
    
    var myContentType = ""
    var myPrivacy = ""
    var haveImage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userIcon.layer.borderWidth = 1
        userIcon.layer.masksToBounds = false
        userIcon.layer.borderColor = UIColor.systemBlue.cgColor
        userIcon.layer.cornerRadius = userIcon.frame.height/2
        userIcon.clipsToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        self.userIcon.image = self.myCurrentUser.userIcon
        
        // blogContent.delegate = self
        
        blogContent.text = "Enter your content"
        blogContent.textColor = UIColor.lightGray
        
        addBtn.isUserInteractionEnabled = false
        addBtn.backgroundColor = UIColor.black
        self.addBtn.setTitle("Enter your content...", for: .normal)
        
        self.userIcon.image = self.myCurrentUser.userIcon
        
        myInitSpeechRecognize()
    }

    @objc func hideKeyboard(tapG:UITapGestureRecognizer){
        self.blogContent.resignFirstResponder()
        self.checkInfo()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        self.checkInfo()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter your content"
            textView.textColor = UIColor.lightGray
        }
        self.checkInfo()
    }
    
    func checkInfo(){
        
        if self.blogContent.text == "Enter your content" || self.blogContent.text.isEmpty{
            self.addBtn.setTitle("Enter your content...", for: .normal)
            self.addBtn.backgroundColor = .black
            self.addBtn.isUserInteractionEnabled = false
            return
        }
        
        self.addBtn.setTitle("Share", for: .normal)
        self.addBtn.backgroundColor = .systemBlue
        self.addBtn.isUserInteractionEnabled = true
    }
    
    @IBAction func addBlogBtn(_ sender: Any) {
        
        let blogUUID = NSUUID().uuidString
        let blogImageString = NSUUID().uuidString
        
        if self.haveImage {
            
            let myKey = ["authorUUID":self.myCurrentUser.uuid, "uuid":blogUUID,
                         "image":blogImageString,"authorImage":self.myCurrentUser.image,
                         "content":self.blogContent.text!, "type":self.myContentType,
                         "authorName":"\(self.myCurrentUser.name)",
                "authorAge":"\(self.myCurrentUser.age)",
                "authorHobby":self.myCurrentUser.hobby,
                "authorGender":self.myCurrentUser.gender,
                "privacy":self.myPrivacy, "dateStr":self.myNowDateFormatter(area: "zh_Hant_HK",
                                                                            formatter: "YYYY/MM/dd HH:mm")]
            
            self.addBlogWithImage(key: myKey, url: self.myNetwork.addBlog, blogImageUUID: blogImageString)
            
            return
        }
        
        let myKey = ["authorUUID":self.myCurrentUser.uuid, "uuid":blogUUID,
                     "image":"null","authorImage":self.myCurrentUser.image,
                     "content":self.blogContent.text!, "type":self.myContentType,
                     "authorName":"\(self.myCurrentUser.name)",
            "authorAge":"\(self.myCurrentUser.age)",
            "authorHobby":self.myCurrentUser.hobby,
            "authorGender":self.myCurrentUser.gender,
            "privacy":self.myPrivacy, "dateStr":self.myNowDateFormatter(area: "zh_Hant_HK",
                                                                        formatter: "YYYY/MM/dd HH:mm")]
        
        self.addBlogWithOutImage(key: myKey, url: self.myNetwork.addBlog)
        
    }
    
    @IBAction func addBlogImage(_ sender: Any) {
        
        self.image.delegate = self
        self.haveImage = true
        
        self.myPerpareAlertAlbumAction()
    }
    
    override func mySelectedAlbumImageTo(image:UIImage){
        self.blogImage.image = image
    }
}

extension AddBlogVC {
    
    func addBlogWithImage(key:[String:String], url:String, blogImageUUID:String){
        
        self.myShowLoadingView(displayView: self.view)
        
        self.mySetStartTime()
        
        guard let data = self.blogImage.image!.jpegData(compressionQuality: 0.1) else {
            return
        }
        
        let storageRef = Storage.storage().reference().child("\(blogImageUUID).jpg")
        let _ = storageRef.putData(data, metadata: nil) { (metadata, error) in
            if error == nil{
                
                self.myAFNetworkRequest(okMessage: "Share blog ok", errorMessage: "share blog error", url: url, httpMethod: .post, header: nil, key: key, encoding: URLEncoding.default, mySuccess: "") { (json) in
                    
                    guard json["value"].stringValue != "successful" else{
                        DispatchQueue.main.async {
                            
                            
                            self.haveImage = true
                            //self.displayAlert(title: "Done", message: "ok")
                            self.myRemoveLoadingView()
                            
                            self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: true, completion: {
                                self.mySetEndTime()
                                self.myCalculateStartAndEndTime(content: "Share onew with image ---")
                            })
                        }
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.displayAlert(title: "Error", message: json["value"].stringValue)
                        self.myRemoveLoadingView()
                    }
                }
            }
        }
    }
    
    func addBlogWithOutImage(key:[String:String], url:String){
        
        self.myAFNetworkRequest(okMessage: "Share blog ok", errorMessage: "share blog error", url: url, httpMethod: .post, header: nil, key: key, encoding: URLEncoding.default, mySuccess: "") { (json) in
            
            self.mySetStartTime()
            
            guard json["value"].stringValue != "successful" else{
                DispatchQueue.main.async {
                    //self.displayAlert(title: "Done", message: "ok")
                    
                    self.navigationController?.popViewController(animated: true)
                                               self.dismiss(animated: true, completion: {
                                            self.mySetEndTime()
                                            self.myCalculateStartAndEndTime(content: "Share onew without image --- ")
                                })
                }
                return
            }
            
            DispatchQueue.main.async {
                self.displayAlert(title: "Error", message: json["value"].stringValue)
            }
        }
    }
}

extension AddBlogVC{
    func myInitSpeechRecognize(){
        
        mySpeechRecognizer?.delegate = self
        SFSpeechRecognizer.requestAuthorization { (status) in
            
            var isStatusOK:Bool
            
            switch status{
                
            case .notDetermined:
                isStatusOK = false
            case .denied:
                isStatusOK = false
            case .restricted:
                isStatusOK = false
            case .authorized:
                isStatusOK = true
            }
            
            OperationQueue.main.addOperation() {
                self.myMicBtn.isEnabled = isStatusOK
            }
        }
    }
    
    func myStartRecording(){
        if myRecognitionTask != nil {
            myRecognitionTask?.cancel()
            myRecognitionTask = nil
        }
        
        let myAudioSession = AVAudioSession.sharedInstance()
        do {
            try myAudioSession.setCategory(AVAudioSession.Category.record)
            try myAudioSession.setMode(AVAudioSession.Mode.measurement)
            try myAudioSession.setActive(false, options: .notifyOthersOnDeactivation)
        } catch {
            self.myPrint(msg: "audioSession properties weren't set because of an error.")
        }
        
        myRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let inputNode:AVAudioInputNode = myAudioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = myRecognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        myRecognitionTask = mySpeechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            var endRecognie = false
            
            if result != nil {
                
                self.blogContent.text = result?.bestTranscription.formattedString
                endRecognie = (result?.isFinal)!
            }
            
            if error != nil || endRecognie {
                self.myAudioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.myRecognitionRequest = nil
                self.myRecognitionTask = nil
                
                self.myMicBtn.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.myRecognitionRequest?.append(buffer)
        }
        
        self.myAudioEngine.prepare()
        
        do {
            try self.myAudioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        self.myMicBtn.setImage(UIImage.init(named: "Mic"), for: .normal)
        self.contentText.text = "Say something, system listening!"
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            self.myMicBtn.isEnabled = true
        } else {
            self.myMicBtn.isEnabled = false
        }
    }
    
    @IBAction func myMicBtnAction(_ sender: AnyObject) {
        if self.myAudioEngine.isRunning {
            self.myAudioEngine.stop()
            self.myRecognitionRequest?.endAudio()
            self.myMicBtn.isEnabled = false
            self.myMicBtn.setImage(UIImage.init(named: "Nomic"), for: .normal)
            self.contentText.text = "Content: "
        } else {
            self.myStartRecording()
            self.myMicBtn.setImage(UIImage.init(named: "Mic"), for: .normal)
            self.contentText.text = "Say something, system listening!"
        }
    }
}

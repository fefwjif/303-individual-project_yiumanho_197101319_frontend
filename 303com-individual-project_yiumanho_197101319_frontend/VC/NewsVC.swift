//
//  NewsVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RSSelectionMenu

class NewsVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    let myNewsURL = "http://newsapi.org/v2/top-headlines"
    let myNewsApiKey = "834b1ada505946ba8ffa6ae75b55c674"
    
    var articlesArr = [MyNewsArticlesModule]()
    var selectedDataArray = [""]
    
    let myCountryArr = ["Hong Kong", "Canada", "Taiwan", "United Kingdom",
                        "United States"]
    let myCategoryArr = ["business", "entertainment","health","science",
                         "sports", "technology", ""]
    
    var selectedContry = "hk"
    var selectedCategory = ""
    
    @IBOutlet weak var myNewTable: UITableView!
    @IBOutlet weak var areaBtn: UIButton!
    @IBOutlet weak var categoryBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myKey:[String:String] = ["country":"hk", "apiKey": myNewsApiKey, "pageSize": "40"]
        
        myGetNews(url: myNewsURL, key: myKey)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articlesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as!NewsTableViewCell
        
        if self.articlesArr[indexPath.row].isLoadedImage {
            cell.NewImage.image = self.articlesArr[indexPath.row].imageContent
        }else{
            Alamofire.request(self.articlesArr[indexPath.row].urlToImage, method: .get).validate().responseData(completionHandler: { (response) in
                if let imageWithData = UIImage(data: response.data!){
                    cell.NewImage.image = imageWithData
                    self.articlesArr[indexPath.row].imageContent = imageWithData
                    self.articlesArr[indexPath.row].isLoadedImage = true
                }else{
                    self.articlesArr[indexPath.row].isLoadedImage = false
                }
            })
        }
        cell.NewImage.layer.cornerRadius = 5
        cell.newTitle.text = self.articlesArr[indexPath.row].source.name
        cell.newDescription.text = self.articlesArr[indexPath.row].title
//      cell.newDate.text = myCovertDateToOtherFormat(oldFormat:  "yyyy-MM-dd'T'HH:mm:ssZ",
//                                                      newFormat: "MMM d, h:mm a",
//                                                      date: self.articlesArr[indexPath.row].publishedAt)
        cell.NewSourceName.text = myCovertDateToOtherFormat(oldFormat:  "yyyy-MM-dd'T'HH:mm:ssZ",
        newFormat: "MMM d, h:mm a",
        date: self.articlesArr[indexPath.row].publishedAt)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    func myGetNews(url:String, key:[String:String]){
        
        self.mySetStartTime()
        
        Alamofire.request(url, method: .get, parameters: key, encoding: URLEncoding.default, headers: nil).responseJSON { (r) in
            if r.result.isSuccess{
                print("Got data from server")
                
                if let newsJSON:JSON = JSON(r.result.value as Any){
                    self.myUpdateNewsModel(j: newsJSON)
                    
                    self.mySetEndTime()
                    self.myCalculateStartAndEndTime(content: "Get news ---")
                }
            }else{
                print("Could not gor data. Error \(String(describing: r.result.error))")
            }
        }
    }
    
    func myUpdateNewsModel(j:JSON){
        let articlesList = j["articles"]
        
        var arr = [MyNewsArticlesModule]()
        for index in 0...articlesList.count - 1{
            let id = articlesList[index]["source"]["id"].stringValue
            let name = articlesList[index]["source"]["name"].stringValue
            
            let author = articlesList[index]["author"].stringValue
            let title = articlesList[index]["title"].stringValue
            let description = articlesList[index]["description"].stringValue
            let url = articlesList[index]["url"].stringValue
            let urlToImage = articlesList[index]["urlToImage"].stringValue
            let publishedAt = articlesList[index]["publishedAt"].stringValue
            
            let content = articlesList[index]["content"].stringValue
            
            arr.append(MyNewsArticlesModule.init(source: MyNewsArticlesSourceModule.init(id:id, name: name),
                                                 author: author, title: title, description: description, url: url,
                                                 urlToImage: urlToImage, publishedAt: publishedAt, content: content, imageContent: nil))
        }
        myCompareDate(arr:arr)
        self.articlesArr = arr
        self.myNewTable.reloadData()
    }
    
    func myCompareDate(arr:[MyNewsArticlesModule]){
        
        arr.sorted { (myNewsArticlesModule, myNewsArticlesModule2) -> Bool in
            let sentDate = Date(fromString:myNewsArticlesModule.publishedAt , format: .isoDateTimeSec)
            let sentDate2 = Date(fromString:myNewsArticlesModule2.publishedAt , format: .isoDateTimeSec)
            
            return sentDate!.compare(sentDate2!) == .orderedDescending
        }
    }
    
    func myCovertDateToOtherFormat(oldFormat:String, newFormat:String, date:String) -> String{
        
        guard let oldDF:DateFormatter = DateFormatter()else {
            return "unknown date"
        }
        oldDF.dateFormat = oldFormat
        
        guard let newDF:DateFormatter = DateFormatter()else {
            return "unknown date"
        }
        
        newDF.dateFormat = newFormat
        
        let date: NSDate? = oldDF.date(from: date) as NSDate?
        
        return newDF.string(from: date! as Date)
    }
    
    
    @IBAction func countryBtn(_ sender: Any) {
        self.myShowSelectionMenu(arr: self.myCountryArr, title: "Conutry", actionName: "ok", height: 300, view: self) { (s) -> () in
            
            switch s{
            case "Hong Knog" :
                self.selectedContry = "hk"
                self.areaBtn.setTitle("Hong Knog", for: .normal)
                
            case "Canada" :
                self.selectedContry = "ca"
                self.areaBtn.setTitle("Canada", for: .normal)
                
            case "Taiwan" :
                self.selectedContry = "tw"
                self.areaBtn.setTitle("Taiwan", for: .normal)
                
            case "China" :
                self.selectedContry = "cn"
                self.areaBtn.setTitle("China", for: .normal)
                
            case "United Kingdom" :
                self.selectedContry = "gb"
                self.areaBtn.setTitle("United Kingdom", for: .normal)
                
            case "United States" :
                self.selectedContry = "us"
                self.areaBtn.setTitle("United States", for: .normal)
                
            default:
                self.selectedContry = "hk"
            }
            
            var myKey:[String:String]?
            
            if self.selectedCategory == ""{
                myKey = ["country":self.selectedContry,
                         "apiKey": self.myNewsApiKey,
                         "pageSize": "40"]
            }else{
                myKey = ["country":self.selectedContry,
                         "apiKey": self.myNewsApiKey,
                         "pageSize": "40", "category":self.selectedCategory]
            }
            
            self.myGetNews(url: self.myNewsURL, key: myKey!)
        }
    }
    
    @IBAction func categoryBtn(_ sender: Any) {
        self.myShowSelectionMenu(arr: self.myCategoryArr, title: "Conutry", actionName: "ok", height: 300, view: self) { (s) -> () in
            self.selectedCategory = s
            
            let myKey = ["country":self.selectedContry,
                         "apiKey": self.myNewsApiKey,
                         "pageSize": "40", "category":self.selectedCategory]
            self.categoryBtn.setTitle(self.selectedCategory, for: .normal)
            self.myGetNews(url: self.myNewsURL, key: myKey)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.myCurrentUser.selectedNews = self.articlesArr[indexPath.row].url

        DispatchQueue.main.async {
            if let details = self.storyboard?.instantiateViewController(withIdentifier: "NewWebVC"){
                self.present(details, animated: true, completion: nil)
            }
        }
    }
    
    public func myShowSelectionMenu(arr:[String], title:String, actionName:String, height: Double,view: UIViewController, mySelectionCallBack: @escaping (String) -> ()){
        // reference from https://github.com/rushisangani/RSSelectionMenu
        
        let mySelectionMenu = RSSelectionMenu(dataSource: arr) { (cell, elemnet, index) in
            cell.textLabel?.text = elemnet
        }
        
        mySelectionMenu.setSelectedItems(items: arr) { [weak self] (elemnet, index, isSelected, selectedElement) in
            
            let element:[String] = selectedElement
            mySelectionCallBack(element[0])
        }
        
        mySelectionMenu.show(style: .actionSheet(title: title, action: actionName,
                                                 height: height), from: self)
    }
}

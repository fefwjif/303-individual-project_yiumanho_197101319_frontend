//
//  RegisterVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase

class RegisterVC: MyBaseVC {
    
    @IBOutlet weak var userAccount: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var RegisterBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mySetRaiseTheView()
    }
    
    
    @IBAction func RegisterBtn(_ sender: Any) {
        
        guard userAccount.text != "", userPassword.text != "" else {
            self.displayAlert(title: "Enter name or password",
                              message: "Enter name or password")
            return
        }
        
        Auth.auth().createUser(withEmail: userAccount.text!, password: userPassword.text!, completion: { (user, error) in
            
            self.mySetStartTime()
            
            if error != nil {
                self.displayAlert(title: "error", message: error!.localizedDescription)
                return
            }
            
           
            
            let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
            changeRequest?.displayName = "no"
            
            changeRequest?.commitChanges(completion: { (error) in
                
                if error != nil {
                    self.myPrint(msg: error?.localizedDescription as Any)
                }
                self.ref.child("users").child((user?.user.uid)!).setValue(["account": "\(self.userAccount.text!)" ,"password": self.userPassword.text!])
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                    
                    self.mySetEndTime()
                    self.myCalculateStartAndEndTime(content: "RegisterBtn() register only")
                    
                    self.afterRegister()
                    self.myPrint(msg: "OK")
                }
            })
        })
    }
    
    func afterRegister(){
        Auth.auth().signIn(withEmail: self.userAccount.text! ,password: self.userPassword.text!, completion: { (user, error) in
            
            guard error == nil else {
                self.displayAlert(title: "error", message: error!.localizedDescription)
                return
            }
            
            guard user != nil else { self.myPrint(msg: "user == nil")
                return }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                //self.myPresentScreenVC(withIdentifier: "FormVC", type: "H")
                if let c = self.storyboard?.instantiateViewController(identifier: "FormVC") as? FormVC{
                    self.navigationController?.pushViewController(c, animated: true)
                }
            }
        })
    }
}

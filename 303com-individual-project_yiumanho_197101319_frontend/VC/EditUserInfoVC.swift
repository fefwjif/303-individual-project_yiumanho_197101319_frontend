//
//  EditUserInfoVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON

class EditUserInfoVC: MyBaseVC {
    
    let regularFont = UIFont.systemFont(ofSize: 22)
    let boldFont = UIFont.boldSystemFont(ofSize: 22)
    
    @IBOutlet weak var editBtnHobbyPicker: UIButton!
    @IBOutlet weak var editBtnGenderPicker: UIButton!
    @IBOutlet weak var editUserNameTF: UITextField!
    @IBOutlet weak var editUserSelfIntroductionTV: UITextView!
    @IBOutlet weak var editUserAgeTF: UITextField!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var editUserIcon: UIImageView!
    @IBOutlet weak var myView:UIView!
    
    var myHobby = ""
    var myGender = ""
    
    var haveImage = false
    var isFinished = [false, false, false, false, false]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.editUserIcon.image = myCurrentUser.userIcon
        self.editUserAgeTF.text = "\(Int((myCurrentUser.age)))"
        self.editUserNameTF.text = myCurrentUser.name
        self.editUserSelfIntroductionTV.text = myCurrentUser.intro
        self.myHobby = myCurrentUser.hobby
        self.myGender = myCurrentUser.gender
        
        self.myCircleImageView(view: [editUserIcon])
        //self.myTFStyle(textFieldArr: [editUserNameTF, editUserAgeTF])
        
        myView.layer.borderColor = UIColor.systemTeal.cgColor
        myView.layer.borderWidth = 1
        myView.layer.cornerRadius = 10
        
        self.mySetRaiseTheView()
    }
    
    @IBAction func openGenderPickerAction(_ sender: Any) {
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "What's your gender?",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : (sender as AnyObject).backgroundColor,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Gender",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : (sender as AnyObject).backgroundColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let arrGender = ["Male", "Female"]
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                self.editBtnGenderPicker.setTitle("\(selectedValue)", for: .normal)
                                                self.isFinished[3] = true
                                                self.myGender = selectedValue
                                            }else{
                                                self.editBtnGenderPicker.setTitle("\(selectedValue)", for: .normal)
                                                self.isFinished[3] = true
                                                self.myGender = selectedValue
                                            }
                                        }else{
                                            self.editBtnGenderPicker.setTitle("What's your gender?", for: .normal)
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
        
    }
    
    
    @IBAction func openHobbyPickerAction(_ sender: Any) {
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "What's your Hobby?",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : (sender as AnyObject).backgroundColor,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Hobby",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : (sender as AnyObject).backgroundColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let arrHobby = ["Focus on Health",
                        "Art", "Dancing", "Golfing", "Volunteering", "Sports",
                        "Fitness activities", "Gardening", "Games",
                        "Caring for a pet", "Writing", "Cooking", "Puzzles",
                        "Book reading","Model building", "Antiquing", "Geneaology",
                        "Bird watching", "Baking",
                        "Travelling", "Community Groups", "Adult Learning"]
        let picker = YBTextPicker.init(with: arrHobby, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrHobby.last!{
                                                self.editBtnHobbyPicker.setTitle("", for: .normal)
                                                self.isFinished[4] = true
                                                self.myHobby = selectedValue
                                            }else{
                                                self.editBtnHobbyPicker.setTitle("\(selectedValue)", for: .normal)
                                                self.isFinished[4] = true
                                                self.myHobby = selectedValue
                                            }
                                        }else{
                                            self.editBtnHobbyPicker.setTitle("What's your Hobby?", for: .normal)
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
    }
    
    @objc func myKBHide(tapG:UITapGestureRecognizer){
        self.editUserNameTF.resignFirstResponder()
        self.editUserAgeTF.resignFirstResponder()
        self.editUserSelfIntroductionTV.resignFirstResponder()
    }
    
    @IBAction func editBtnAction(_ sender: Any) {
        
        let editIconString = NSUUID().uuidString
        
        if self.haveImage {
            
            let update = ["/users/\(Auth.auth().currentUser!.uid)/name": "\(self.editUserNameTF.text ?? "user")", "/users/\(Auth.auth().currentUser!.uid)/image": editIconString]
            
            let key = ["oname":self.myCurrentUser.name, "oage":"\(self.myCurrentUser.age)",
                "ointroduction":self.myCurrentUser.intro, "ohobby":self.myCurrentUser.hobby,
                "oimage":self.myCurrentUser.image, "ogender":self.myCurrentUser.gender,
                "uuid":Auth.auth().currentUser!.uid, "name":self.editUserNameTF.text!,
                "age":"\(self.editUserAgeTF.text!)", "introduction":self.editUserSelfIntroductionTV.text!,
                "hobby":self.myHobby, "image":editIconString, "gender":self.myGender] as! [String : String]
            
            self.editUserWithIcon(key: key as! [String : String], url: self.myNetwork.editUser, update: update, changedImage: editIconString)
        }else{
            
            let key = ["oname":self.myCurrentUser.name, "oage":"\(self.myCurrentUser.age)",
                "ointroduction":self.myCurrentUser.intro, "ohobby":self.myCurrentUser.hobby,
                "oimage":self.myCurrentUser.image, "ogender":self.myCurrentUser.gender,
                "uuid":Auth.auth().currentUser!.uid, "name":self.editUserNameTF.text!,
                "age":"\(self.editUserAgeTF.text!)", "introduction":self.editUserSelfIntroductionTV.text!,
                "hobby":self.myHobby, "image":self.myCurrentUser.image, "gender":self.myGender] as! [String : String]
            
            let update = ["/users/\(Auth.auth().currentUser!.uid)/name": "\(self.editUserNameTF.text ?? "user")", "/users/\(Auth.auth().currentUser!.uid)/image": self.myCurrentUser.image]
            
            self.editUserWithoutIcon(key: key, url: self.myNetwork.editUser, update: update)
        }
    }
    
    @IBAction func addUserIconBtn(_ sender: Any) {
        self.haveImage = true
        
        self.myPerpareAlertAlbumAction()
    }
    
    override func mySelectedAlbumImageTo(image: UIImage) {
        self.editUserIcon.image = image
    }
}

extension EditUserInfoVC{
    
    func editUserWithIcon(key:[String:String], url:String, update:[String:Any], changedImage:String){
        
        guard let data = self.editUserIcon.image!.jpegData(compressionQuality: 0.1) else {
            return
        }
        
        let storageRef = Storage.storage().reference().child("users").child("icon").child("\(Auth.auth().currentUser!.uid).jpg")
        let _ = storageRef.putData(data, metadata: nil) { (metadata, error) in
            if error == nil{
                self.myShowLoadingView(displayView: self.view)
                
                guard let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() else { return }
                changeRequest.displayName = "Yes"
                changeRequest.commitChanges(completion: { (error) in
                    guard error == nil else { self.myPrint(msg: error?.localizedDescription as Any)
                        return
                    }
                    
                    DispatchQueue.main.async(){
                        self.myAFNetworkRequest(okMessage: "editUserWithIcon ok", errorMessage: "editUserWithIcon error", url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
                            
                            if json["value"].stringValue != "successful"{
                                self.displayAlert(title: "ERROR", message: "\(json["value"].stringValue)")
                            }else{
                                self.myRemoveLoadingView()
                                self.displayAlert(title: "Done", message: "ok")
                                
                                self.myCurrentUser.name = self.editUserNameTF.text!
                                self.myCurrentUser.age =  Double(self.editUserAgeTF.text!)!
                                self.myCurrentUser.intro = self.editUserSelfIntroductionTV.text!
                                self.myCurrentUser.hobby = self.myHobby
                                self.myCurrentUser.image = changedImage
                                self.myCurrentUser.gender = self.myGender
                            }
                            
                        }
                    }
                })
                
            }
        }
    }
    
    func editUserWithoutIcon (key:[String:String], url:String, update:[String:Any]){
        self.myShowLoadingView(displayView: self.view)
        guard let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() else { return }
        changeRequest.displayName = "Yes"
        changeRequest.commitChanges(completion: { (error) in
            guard error == nil else { self.myPrint(msg: error?.localizedDescription as Any)
                return
            }
            
            self.ref.updateChildValues(update)
            
            DispatchQueue.main.async(){
                
                self.myAFNetworkRequest(okMessage: "editUserWithIcon ok", errorMessage: "editUserWithIcon error", url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
                    
                    if json["value"].stringValue != "successful"{
                        self.displayAlert(title: "ERROR", message: "\(json["value"].stringValue)")
                    }else{
                        self.myRemoveLoadingView()
                        self.displayAlert(title: "Done", message: "ok")
                        
                        self.myCurrentUser.name = self.editUserNameTF.text!
                        self.myCurrentUser.age =  Double(self.editUserAgeTF.text!)!
                        self.myCurrentUser.intro = self.editUserSelfIntroductionTV.text!
                        self.myCurrentUser.hobby = self.myHobby
                        self.myCurrentUser.image = "null"
                        self.myCurrentUser.gender = self.myGender
                    }
                }
            }
        })
    }
}


//
//  ForgetPassVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 25/5/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase

class ForgetPassVC: MyBaseVC {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var sendEmial: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func resetPassword(_ sender: Any) {
        if email.text != ""{
            Auth.auth().sendPasswordReset(withEmail: email.text!) { error in
            
            if error != nil {
                print(error!.localizedDescription)
                
                self.displayAlert(title: "Error", message: (error!.localizedDescription))
                
                return
            } else {
                self.displayAlert(title: "OK", message: "ok")
                 }
            }
        }else{
            self.displayAlert(title: "Error", message: "enter email")
        }
    }

}

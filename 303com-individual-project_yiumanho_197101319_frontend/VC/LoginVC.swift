//
//  LoginVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Alamofire
import SwiftyJSON

@available(iOS 13.0, *)
class LoginVC: MyBaseVC {
    
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var userPasswordTF: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var myView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //myView.layer.cornerRadius = 10
        //myTFStyle(textFieldArr: [userNameTF, userPasswordTF])
        
        mySetRaiseTheView()
        mySignIn()
    }
    
    func mySignIn(){
        self.authStateListenerHandle = Auth.auth().addStateDidChangeListener { (auth, user) in
            
            if let user = user { // If user has login
                
                self.mySetStartTime()
                
                self.myPrint(msg: "\(String(describing: user.email))")
                
                guard user.displayName == "Yes" else {
                    self.myRemoveLoadingView()
                    //self.myPresentScreenVC(withIdentifier: "FormVC", type: "F")
                    if let c = self.storyboard?.instantiateViewController(identifier: "FormVC") as? FormVC{
                        self.navigationController?.pushViewController(c, animated: true)
                    }
                    return }
                
                DispatchQueue.main.async {
                    self.myShowLoadingView(displayView: self.view)
                    
                    self.myAFNetworkRequest(okMessage: "login ok", errorMessage: "login error", url: self.myNetwork.getUserFromUUID, httpMethod: .post, header: nil, key: ["uuid":user.uid], encoding: URLEncoding.default, mySuccess: "") { (json) in
                        
                        self.myGoToMainPages(json: json)
                    }
                }
            }
        }
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        Auth.auth().signIn(withEmail: self.userNameTF.text! ,password: self.userPasswordTF.text!, completion: { (user, error) in
            
            
            
            self.myShowLoadingView(displayView: self.view)
            guard error == nil else {
                self.displayAlert(title: "error", message: error!.localizedDescription)
                self.myRemoveLoadingView()
                return
            }
            
            guard user != nil else { self.myPrint(msg: "user == nil \(self.userNameTF.text!)")
                self.myRemoveLoadingView()
                return }
            
            guard user!.user.displayName == "Yes" else {
                self.myRemoveLoadingView()
                //self.myPresentScreenVC(withIdentifier: "FormVC", type: "F")
                    if let c = self.storyboard?.instantiateViewController(identifier: "FormVC") as? FormVC{
                        self.navigationController?.pushViewController(c, animated: true)
                    }
                return }
            
            self.myAFNetworkRequest(okMessage: "login ok", errorMessage: "login error", url: self.myNetwork.getUserFromUUID, httpMethod: .post, header: nil, key: ["uuid":user!.user.uid], encoding: URLEncoding.default, mySuccess: "") { (json) in
                
                self.myGoToMainPages(json: json)
            }
        })
    }
    
    @IBAction func ResetPasswordBtn(_ sender: Any) {
        if let c = storyboard?.instantiateViewController(identifier: "ForgetPassVC") as? ForgetPassVC{
            navigationController?.pushViewController(c, animated: true)
        }
    }
    
    @IBAction func RegisterBtn(_ sender: Any) {

        if let c = storyboard?.instantiateViewController(identifier: "RegisterVC") as? RegisterVC{
            navigationController?.pushViewController(c, animated: true)
        }
    }
}

@available(iOS 13.0, *)
extension LoginVC{
    func myGoToMainPages(json:JSON){
        self.myPrint(msg: json)
                
        let value = json["value"]
        
        self.myCurrentUser.name = value["name"].stringValue
        self.myCurrentUser.age = value["age"].doubleValue
        self.myCurrentUser.uuid = value["uuid"].stringValue
        self.myCurrentUser.hobby = value["hobby"].stringValue
        self.myCurrentUser.gender = value["gender"].stringValue
        self.myCurrentUser.image = value["image"].stringValue
        self.myCurrentUser.intro = value["introduction"].stringValue
        for friend in value["friends"] {
            self.myCurrentUser.friends.append(friend.1.stringValue)
        }
        DispatchQueue.main.async {
            self.myRemoveLoadingView()
            
            self.mySetEndTime()
            self.myCalculateStartAndEndTime(content: "Login --- ")
            
            self.myPresentScreenVC(withIdentifier: "MainUITabBarController", type: "F")
        }
    }
}

//
//  MyCommentVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 12/5/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import Alamofire

class MyCommentVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    var myCommentModule = [MyCommentModule]()
    
    var myKey:[String:Any]?
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var commentContent:UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        myKey = ["blogUUID":self.myCurrentUser.selectedBlogUUID]
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        self.getBlogComment(url: self.myNetwork.getBlogComment,
                                   key: myKey!,
                                   method: .post, okMsg: "getBlogComment ok",
                                   errorMsg: "getBlogComment error", myReload: {
                                    
                                    self.myTableView.reloadData()
                                    self.myRefreshControl.endRefreshing()
        })
        
        commentContent.text = "Enter comment"
        commentContent.textColor = UIColor.lightGray
        
        self.commentBtn.setTitleColor(.black, for: .normal)
        self.commentBtn.isEnabled = false
        
        mySetRaiseTheView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        myCommentModule.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommenttCell", for: indexPath) as! CommenttCell
        
        self.myCircleImageView(view: [cell.commentUserIcon])
        
//        if (self.myCommentModule[indexPath.row].iconLink != "null"){
            if self.myCommentModule[indexPath.row].isLoadedIcon {
                           cell.commentUserIcon.image = self.myCommentModule[indexPath.row].iconContent
                       }else{
                Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(myCommentModule[indexPath.row].commenterUUID).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                               if error != nil {
                                   self.myPrint(msg: error!.localizedDescription)
                                   self.myCommentModule[indexPath.row].iconContent = UIImage(named: "User")!
                                   self.myCommentModule[indexPath.row].isLoadedIcon = true
                                   cell.commentUserIcon.image = self.myCommentModule[indexPath.row].iconContent
                                   return
                               }
                               if let d = data{
                                   self.myCommentModule[indexPath.row].iconContent = UIImage.init(data: d)!
                                   self.myCommentModule[indexPath.row].isLoadedIcon = true
                                   cell.commentUserIcon.image = self.myCommentModule[indexPath.row].iconContent
                               }
                           })
                }
//        }else{
//            cell.commentUserIcon.image = UIImage(named: "User")!
//        }
        cell.commentContent.text = self.myCommentModule[indexPath.row].comment
        cell.commentUserName.text = self.myCommentModule[indexPath.row].commentUserName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    override func myRefreshTableAction() {
        self.getBlogComment(url: self.myNetwork.getBlogComment,
                                   key: myKey!,
                                   method: .post, okMsg: "getBlogComment ok",
                                   errorMsg: "getBlogComment error", myReload: {
                                    
                                    self.myTableView.reloadData()
                                    self.myRefreshControl.endRefreshing()
        })
    }
    
    @IBAction func addCommentBtn(_ sender: Any) {
           
           let commentUUID = NSUUID().uuidString
           
           let key = ["commenterUUID":self.myCurrentUser.uuid,
                      "uuid":commentUUID,
                      "commenterImage":self.myCurrentUser.image,
                      "blogUUID":self.myCurrentUser.selectedBlogUUID,
                      "comment":self.commentContent.text ?? "",
                      "commenterName":self.myCurrentUser.name,
                    "dateStr":self.myNowDateFormatter(area: "zh_Hant_HK",
                                                                           formatter: "YYYY/MM/dd HH:mm")]
           self.addComment(key: key, url: self.myNetwork.addBlogComment)
       }
    
    override func mySetKBShowAction(){
        myKBShowAction(mySize: -280)
    }
    
}

extension MyCommentVC{
    func getBlogComment(url:String, key:[String:Any], method:HTTPMethod, okMsg:String, errorMsg:String, myReload: @escaping  ()->()){
        
        self.myAFNetworkRequest(okMessage: okMsg, errorMessage: errorMsg, url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
            
            self.mySetStartTime()
            
            DispatchQueue.main.async {
                self.myPrint(msg: json)
                self.myHandleJson(json: json, type: "r")
                
                self.mySetEndTime()
                self.myCalculateStartAndEndTime(content: "Get comment --- ")
 
                myReload()
            }
        }
    }
    
    func myHandleJson(json:JSON, type:String){
        let commentJsonArr = json["value"]
        var commentModule = [MyCommentModule]()

        guard commentJsonArr.count != 0 else{
            self.myCommentModule = [MyCommentModule]()
            self.myRefreshControl.endRefreshing()
            return
        }
        
        for index in 0...commentJsonArr.count - 1{
            
            commentModule.append(MyCommentModule.init(uuid: commentJsonArr[index]["uuid"].stringValue,
            iconLink: commentJsonArr[index]["commenterImage"].stringValue,
            isLoadedIcon: false,
            commentUserName: commentJsonArr[index]["commenterName"].stringValue,
            dateStr: commentJsonArr[index]["dateStr"].stringValue,
            date: commentJsonArr[index]["dateStr"].doubleValue,
            comment: commentJsonArr[index]["comment"].stringValue,
            blogUUID: commentJsonArr[index]["blogUUID"].stringValue,
            commenterUUID: commentJsonArr[index]["commenterUUID"].stringValue))
            
            
            self.myCommentModule = commentModule
        }
    }
    
    func addComment(key:[String:String], url:String){
        
        self.myAFNetworkRequest(okMessage: "comment ok", errorMessage: "comment error", url: url, httpMethod: .post, header: nil, key: key, encoding: URLEncoding.default, mySuccess: "") { (json) in
            
            self.mySetStartTime()
            
            guard json["value"].stringValue != "successful" else{
                DispatchQueue.main.async {
                    //self.displayAlert(title: "Done", message: "ok")
                    
//                    self.navigationController?.popViewController(animated: true)
//                                               self.dismiss(animated: true, completion: {
//                    self.mySetEndTime()
//                    self.myCalculateStartAndEndTime(content: "add comment --- ")
//
//                                })
                    
                    self.myToast(content: "OK")
                    self.myCommentModule.insert(MyCommentModule.init(uuid: self.myCurrentUser.uuid,
                    iconLink: self.myCurrentUser.image, isLoadedIcon: false,
                    commentUserName: self.myCurrentUser.name, dateStr: self.myNowDateFormatter(area: "zh_Hant_HK",
                                                                                               formatter: "YYYY/MM/dd HH:mm"), date: 123141,
                                                                                                                               comment: self.commentContent.text, blogUUID: "awkdnakwdhuakwbnjd", commenterUUID: self.myCurrentUser.uuid), at: 0)
                    self.myTableView.reloadData()
                }
                return
            }
            
            DispatchQueue.main.async {
                self.displayAlert(title: "Error", message: json["value"].stringValue)
            }
        }
    }
    
    @objc func hideKeyboard(tapG:UITapGestureRecognizer){
        self.commentContent.resignFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Enter comment"
            textView.textColor = UIColor.lightGray
            self.commentBtn.setTitleColor(.black, for: .normal)
            self.commentBtn.isEnabled = false
        }else{
            self.commentBtn.setTitleColor(.systemBlue, for: .normal)
            self.commentBtn.isEnabled = true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        self.commentBtn.setTitleColor(.systemBlue, for: .normal)
        self.commentBtn.isEnabled = true
        }
    }
}

//
//  MyBlogsListVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import Alamofire

class MyBlogsListVC: MyBaseVC,UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var myTableView: UITableView!
    
    var myBlogsModule = [MyBlogModule]()
    
    var myKey:[String:String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myKey = ["authorUUID":self.myCurrentUser.uuid]
        
        myTableView.addSubview(myRefreshControl)
        
        self.getMyBlog(url: self.myNetwork.myBlogs, key: myKey!, okMsg: "getMyBlog ok", errorMsg: "getMyBlog error", myReload: {
            self.myTableView.reloadData()
        })
    }
    
    override func myRefreshTableAction() {
        self.getMyBlog(url: self.myNetwork.myBlogs, key: myKey!, okMsg: "getMyBlog ok", errorMsg: "getMyBlog error", myReload: {
            self.myTableView.reloadData()
            self.myRefreshControl.endRefreshing()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myBlogsModule.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if myBlogsModule[indexPath.row].image != "null"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCellWithImage", for: indexPath) as!BlogCellWithImage
            
            self.myCircleImageView(view: [cell.userImage])
            //self.myTVStyle(textViewArr: [cell.blogContent])
            self.myCellImageView(view: [cell.blogImage], i: 10)
            
            if myBlogsModule[indexPath.row].isLoadedImage {
                cell.blogImage.image = self.myBlogsModule[indexPath.row].imageContent
            }else{
                Storage.storage().reference(forURL: self.myNetwork.fbs).child("\(self.myBlogsModule[indexPath.row].image).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                    if error != nil {
                        self.myPrint(msg: error!.localizedDescription)
                        self.myBlogsModule[indexPath.row].imageContent = UIImage(named: "User")!
                        self.myBlogsModule[indexPath.row].isLoadedImage = false
                        cell.blogImage.image = self.myBlogsModule[indexPath.row].imageContent
                        return
                    }
                    if let d = data{
                        self.myBlogsModule[indexPath.row].imageContent = UIImage.init(data: d)!
                        self.myBlogsModule[indexPath.row].isLoadedImage = true
                        cell.blogImage.image = self.myBlogsModule[indexPath.row].imageContent
                    }
                })
            }
            
            cell.userName.text = self.myBlogsModule[indexPath.row].authorName
            cell.blogContent.text = self.myBlogsModule[indexPath.row].content
            cell.userImage.image = self.myCurrentUser.userIcon
            cell.blogDate.text = self.myBlogsModule[indexPath.row].dateStr
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCellWithoutImage", for: indexPath) as!BlogCellWithoutImage
            
            self.myCircleImageView(view: [cell.userImage])
           // self.myTVStyle(textViewArr: [cell.blogContent])
            
            cell.userName.text = self.myBlogsModule[indexPath.row].authorName
            cell.blogContent.text = self.myBlogsModule[indexPath.row].content
            cell.userImage.image = self.myCurrentUser.userIcon
            cell.blogDate.text = self.myBlogsModule[indexPath.row].dateStr
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.myBlogsModule[indexPath.row].image != "null" {
            return 380 + 50 + 15
        }else{
            return 220 + 15
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
           if section == 0 {
               if self.myBlogsModule.count == 0 {
                   return "There is no your post"
               }else{
                   return "Your posts"
               }
           }else{
               return ""
           }
       }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            
            let myKey = ["uuid":self.myBlogsModule[indexPath.row].uuid]
            
            self.myAFNetworkRequest(okMessage: "remove blog ok", errorMessage: "remove blog erro",
                                         url: self.myNetwork.deleteBlogs, httpMethod: .post, header: nil,
                                         key: myKey, encoding: JSONEncoding.default,
                                         mySuccess: "") { (json) in
                                            
                                            if json["value"].stringValue == "successful" {
                                                
                                                self.myBlogsModule.remove(at: indexPath.row)
                                                self.myTableView.reloadData()
                                                self.displayAlert(title: "Done", message: "ok")
                                                
                                            }else{
                                                self.displayAlert(title: "error", message: "error")
                                            }
            }
        }
    }
    
    func getMyBlog(url:String, key:[String:Any], okMsg:String, errorMsg:String, myReload: @escaping  ()->()){
        
        self.myAFNetworkRequest(okMessage: okMsg, errorMessage: errorMsg, url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
            DispatchQueue.main.async {
                let blogJsonArr = json["value"]
                var blogModule = [MyBlogModule]()
                
                guard blogJsonArr.count != 0 else{
                    self.myBlogsModule = [MyBlogModule]()
                    myReload()
                    return
                }
                
                self.myPrint(msg: blogJsonArr)
                
                for index in 0...blogJsonArr.count - 1{
                    blogModule.append(MyBlogModule.init(uuid: blogJsonArr[index]["uuid"].stringValue,
                                                        authorUUID: blogJsonArr[index]["authorUUID"].stringValue,
                                                        image: blogJsonArr[index]["image"].stringValue,
                                                        authorImage: blogJsonArr[index]["authorImage"].stringValue,
                                                        content: blogJsonArr[index]["content"].stringValue,
                                                        date: blogJsonArr[index]["date"].doubleValue,
                                                        privacy: blogJsonArr[index]["privacy"].stringValue,
                                                        type: blogJsonArr[index]["type"].stringValue,
                                                        authorName: blogJsonArr[index]["authorName"].stringValue,
                                                        authorAge: blogJsonArr[index]["authorAge"].doubleValue,
                                                        authorHobby: blogJsonArr[index]["authorHobby"].stringValue,
                                                        authorGender: blogJsonArr[index]["authorGender"].stringValue,
                                                        dateStr: blogJsonArr[index]["dateStr"].stringValue,
                                                        isLoadedImage: false, isLoadedIcon: false))
                }
                
                self.myBlogsModule = blogModule
                myReload()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
           self.myCurrentUser.selectedBlogUUID = self.myBlogsModule[indexPath.row].uuid
           
           if let c = storyboard?.instantiateViewController(identifier: "MyCommentVC") as? MyCommentVC{
               navigationController?.pushViewController(c, animated: true)
           }
       }
}

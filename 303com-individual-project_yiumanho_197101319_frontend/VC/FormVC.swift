//
//  FormVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import Alamofire
import SwiftyJSON

class FormVC: MyBaseVC {
    
    let regularFont = UIFont.systemFont(ofSize: 22)
    let boldFont = UIFont.boldSystemFont(ofSize: 22)
    
    @IBOutlet weak var btnHobbyPicker: UIButton!
    @IBOutlet weak var btnGenderPicker: UIButton!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var userSelfIntroductionTV: UITextView!
    @IBOutlet weak var userAgeTF: UITextField!
    @IBOutlet weak var comfirmBtn: UIButton!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var myView:UIView!
    
    var myHobby = ""
    var myGender = ""
    
    var haveImage = false
    var isFinished = [false, false, false, false, false]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        self.myCircleImageView(view: [userIcon])
        
        self.mySetRaiseTheView()
        
        self.checkInfo()
    }
    
    @IBAction func openGenderPickerAction(_ sender: Any) {
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "What's your gender?",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : (sender as AnyObject).backgroundColor,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Gender",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : (sender as AnyObject).backgroundColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let arrGender = ["Male", "Female"]
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                self.btnGenderPicker.setTitle("\(selectedValue)", for: .normal)
                                                self.isFinished[3] = true
                                                self.myGender = selectedValue
                                                self.checkInfo()
                                            }else{
                                                self.btnGenderPicker.setTitle("\(selectedValue)", for: .normal)
                                                self.isFinished[3] = true
                                                self.myGender = selectedValue
                                                self.checkInfo()
                                            }
                                        }else{
                                            self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
        
        self.checkInfo()
    }
    
    
    @IBAction func openHobbyPickerAction(_ sender: Any) {
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "What's your Hobby?",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : (sender as AnyObject).backgroundColor,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search Hobby",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : (sender as AnyObject).backgroundColor,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let arrHobby = ["Focus on Health",
                        "Art", "Dancing", "Golfing", "Volunteering", "Sports",
                        "Fitness activities", "Gardening", "Games",
                        "Caring for a pet", "Writing", "Cooking", "Puzzles",
                        "Book reading","Model building", "Antiquing", "Geneaology",
                        "Bird watching", "Baking",
                        "Travelling", "Community Groups", "Adult Learning"]
        let picker = YBTextPicker.init(with: arrHobby, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrHobby.last!{
                                                self.btnHobbyPicker.setTitle("", for: .normal)
                                                self.isFinished[4] = true
                                                self.myHobby = selectedValue
                                                self.checkInfo()
                                            }else{
                                                self.btnHobbyPicker.setTitle("\(selectedValue)", for: .normal)
                                                self.isFinished[4] = true
                                                self.myHobby = selectedValue
                                                self.checkInfo()
                                            }
                                        }else{
                                            self.btnHobbyPicker.setTitle("What's your Hobby?", for: .normal)
                                            self.checkInfo()
                                        }
        },
                                       onCancel: {
                                        print("Cancelled")
        }
        )
        
        picker.show(withAnimation: .FromBottom)
    }
    
    @objc func hideKeyboard(tapG:UITapGestureRecognizer){
        self.userNameTF.resignFirstResponder()
        self.userAgeTF.resignFirstResponder()
        self.userSelfIntroductionTV.resignFirstResponder()
        self.checkInfo()
    }
    
    func checkInfo(){
        
        if self.userNameTF.text != "" && self.userAgeTF.text != "" && self.userSelfIntroductionTV.text != "" {
            self.isFinished[0] = true
            self.isFinished[1] = true
            self.isFinished[2] = true
        }else{
            self.isFinished[0] = false
            self.isFinished[1] = false
            self.isFinished[2] = false
        }
        
        guard self.isFinished.contains(false) else {
            self.comfirmBtn.setTitle("Go", for: .normal)
            self.comfirmBtn.backgroundColor = .systemBlue
            self.comfirmBtn.isUserInteractionEnabled = true
            return
        }
        
        self.comfirmBtn.setTitle("Please enter all information", for: .normal)
        self.comfirmBtn.backgroundColor = .black
        self.comfirmBtn.isUserInteractionEnabled = false
    }
    
    
    @IBAction func enterMainPage(_ sender: Any) {
        if self.haveImage {
            
            let uuidStr = NSUUID().uuidString
            
            let update = ["/users/\(Auth.auth().currentUser!.uid)/name": "\(self.userNameTF.text ?? "user")", "/users/\(Auth.auth().currentUser!.uid)/image": uuidStr]
            
            let key = ["name":self.userNameTF.text!, "age":self.userAgeTF.text!, "introduction": self.userSelfIntroductionTV.text!, "hobby":self.myHobby, "image":uuidStr, "gender":self.myGender,
                       "uuid":Auth.auth().currentUser!.uid]
            
            self.withIcon(key: key, url: self.myNetwork.initUser, update: update, uuidStr: uuidStr)
            
        }else{
            
            let key = ["name":self.userNameTF.text!, "age":self.userAgeTF.text!, "introduction": self.userSelfIntroductionTV.text!, "hobby":self.myHobby, "image":"null", "gender":self.myGender,
                       "uuid":Auth.auth().currentUser!.uid]
            
            let update = ["/users/\(Auth.auth().currentUser!.uid)/name": "\(self.userNameTF.text ?? "user")", "/users/\(Auth.auth().currentUser!.uid)/image": "null"]
            
            self.withoutIcon(key: key, url: self.myNetwork.initUser, update: update)
        }
    }
    
    @IBAction func addUserIconBtn(_ sender: Any) {
        self.haveImage = true
        self.myPerpareAlertAlbumAction()
    }
    
    override func mySelectedAlbumImageTo(image:UIImage) {
        self.userIcon.image = image
    }
}

extension FormVC {
    
    func withIcon(key:[String:String], url:String, update:[String:Any], uuidStr:String){
        
        
        guard let data = self.userIcon.image!.jpegData(compressionQuality: 0.1) else {
            return
        }
        
        let storageRef = Storage.storage().reference().child("users").child("icon").child("\(Auth.auth().currentUser!.uid).jpg")
        let _ = storageRef.putData(data, metadata: nil) { (metadata, error) in
            if error == nil{
                self.myShowLoadingView(displayView: self.view)
                
                guard let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() else { return }
                changeRequest.displayName = "Yes"
                changeRequest.commitChanges(completion: { (error) in
                    guard error == nil else { self.myPrint(msg: error?.localizedDescription as Any)
                        return
                    }
                    
                    self.ref.updateChildValues(update) { (error, DatabaseReference) in
                        if error == nil {
                            self.myAFNetworkRequest(okMessage: "form ok", errorMessage: "form error", url: url, httpMethod: .post, header: nil, key: key, encoding: URLEncoding.default, mySuccess: "") { (json) in
                                
                                DispatchQueue.main.async(){
                                    if json["value"].stringValue != "successful"{
                                        self.displayAlert(title: "ERROR", message: "\(json["ok"].stringValue)")
                                        self.myRemoveLoadingView()
                                    }else{
                                    
                                        self.myGoToMainPages(json: json, imageID: uuidStr)
                                        self.myRemoveLoadingView()
                                        
                                        self.myPresentScreenVC(withIdentifier: "MainUITabBarController", type: "F")
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    func withoutIcon (key:[String:String], url:String, update:[String:Any]){
        self.myShowLoadingView(displayView: self.view)
                
        guard let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest() else { return }
        changeRequest.displayName = "Yes"
        changeRequest.commitChanges(completion: { (error) in
            guard error == nil else { self.myPrint(msg: error?.localizedDescription as Any)
                return
            }
            
            self.ref.updateChildValues(update) { (error, DatabaseReference) in
                if error == nil {
                    self.myAFNetworkRequest(okMessage: "form ok", errorMessage: "form error", url: url, httpMethod: .post, header: nil, key: key, encoding: URLEncoding.default, mySuccess: "") { (json) in
                                   
                                   DispatchQueue.main.async(){
                                       if json["value"].stringValue == "successful"{
                                           self.myGoToMainPages(json: json, imageID: "null")
                                                                 self.myRemoveLoadingView()
                                                                 self.myPresentScreenVC(withIdentifier: "MainUITabBarController", type: "F")
                                       }else{
                                           self.displayAlert(title: "ERROR", message: "\(json["ok"].stringValue)")
                                           self.myRemoveLoadingView()
                         }
                      }
                    }
                }
            }
        })
    }
}

extension FormVC{
    func myGoToMainPages(json:JSON, imageID:String){
        self.myPrint(msg: json)
        self.myCurrentUser.name = self.userNameTF.text!
        self.myCurrentUser.age = Double (self.userAgeTF.text!)!
        self.myCurrentUser.uuid = Auth.auth().currentUser!.uid
        self.myCurrentUser.hobby = self.myHobby
        self.myCurrentUser.gender = self.myGender
        self.myCurrentUser.image = imageID
        self.myCurrentUser.intro = self.userSelfIntroductionTV.text!
        self.myCurrentUser.friends = [String]()
    }
}

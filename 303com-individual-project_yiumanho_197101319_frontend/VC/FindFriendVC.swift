//
//  FindFriendVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import Alamofire

class FindFriendVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    var mySearchController: UISearchController!
    var myShowSearchResult: Bool = false
    
    var myRecommendedFriends = [MyUserModule]()
    var mySearchFriends = [MyUserModule]()
    
    var mySearching = false
    var mySearchName = ""
    
    var myKey:[String:Any]?
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mySearchController = UISearchController(searchResultsController: nil)
        self.mySearchController.searchBar.placeholder = "Search friends"
        self.mySearchController.searchBar.sizeToFit()
        self.mySearchController.searchResultsUpdater = self
        self.mySearchController.searchBar.delegate = self
        self.mySearchController.dimsBackgroundDuringPresentation = false
        self.myTableView.tableHeaderView = self.mySearchController.searchBar
        
        myTableView.addSubview(myRefreshControl)
        
       // self.title = "People have common hobby"
        
        myKey = ["friends":self.myCurrentUser.friends,"uuid":self.myCurrentUser.uuid,"hobby":self.myCurrentUser.hobby]
        
        self.getRecommendedFriends(url: self.myNetwork.getRecommendedFriends,
                                   key: myKey!,
                                   method: .post, okMsg: "getRecommendedFriends ok",
                                   errorMsg: "getRecommendedFriends error", myReload: {
                                    
                                    self.mySearching = false
                                    self.myTableView.reloadData()
                                    self.myRefreshControl.endRefreshing()
        })
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
           if section == 0 {
            if !mySearching {
                if self.myRecommendedFriends.count == 0 {
                                  return "No friend can recommend to you"
                              }else{
                                  return "System recommended some friend to you"
                              }
            }else{
                return ""
            }
           }else{
               return ""
           }
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mySearching{
            return self.mySearchFriends.count
        }else{
            return self.myRecommendedFriends.count
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.mySearchName = searchBar.text ?? ""
        self.getUserFromName(url: self.myNetwork.getFriendsFromName, userName: self.mySearchName, method: .post,
                             okMsg: "getUserFromName ok", errorMsg: "getUserFromName error", myReload: {
                                
                                self.mySearching = true
                                self.myTableView.reloadData()
                                self.myRefreshControl.endRefreshing()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !mySearching {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell", for: indexPath) as! FriendCell
            
            cell.userIcon.layer.borderWidth = 2
            cell.userIcon.layer.masksToBounds = false
            cell.userIcon.layer.borderColor = UIColor.systemBlue.cgColor
            cell.userIcon.layer.cornerRadius = cell.userIcon.frame.height/2
            cell.userIcon.clipsToBounds = true
            
//            if myRecommendedFriends[indexPath.row].icon != "null"{
                
                if myRecommendedFriends[indexPath.row].isLoadedIcon {
                    cell.userIcon.image = self.myRecommendedFriends[indexPath.row].iconContent
                }else{
                    Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(self.myRecommendedFriends[indexPath.row].uuid).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                        if error != nil {
                            self.myPrint(msg: error!.localizedDescription)
                            self.myRecommendedFriends[indexPath.row].iconContent = UIImage(named: "User")!
                            self.myRecommendedFriends[indexPath.row].isLoadedIcon = false
                            cell.userIcon.image = self.myRecommendedFriends[indexPath.row].iconContent
                            return
                        }
                        if let d = data{
                            self.myRecommendedFriends[indexPath.row].iconContent = UIImage.init(data: d)!
                            self.myRecommendedFriends[indexPath.row].isLoadedIcon = true
                            cell.userIcon.image = self.myRecommendedFriends[indexPath.row].iconContent
                        }
                    })
                }
                
//            }else {
//                cell.userIcon.image = UIImage(named: "User")!
//            }
            
            cell.userName.text = self.myRecommendedFriends[indexPath.row].name
            cell.userHobby.text = "Age: \(Int(self.myRecommendedFriends[indexPath.row].age))"
            cell.userAge.text = self.myRecommendedFriends[indexPath.row].gender
            
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell", for: indexPath) as! FriendCell
            
            cell.userIcon.layer.borderWidth = 2
            cell.userIcon.layer.masksToBounds = false
            cell.userIcon.layer.borderColor = UIColor.systemBlue.cgColor
            cell.userIcon.layer.cornerRadius = cell.userIcon.frame.height/2
            cell.userIcon.clipsToBounds = true
            
            if self.mySearchFriends[indexPath.row].icon != "null"{
                
                if mySearchFriends[indexPath.row].isLoadedIcon {
                    cell.userIcon.image = self.mySearchFriends[indexPath.row].iconContent
                }else{
                    Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(self.mySearchFriends[indexPath.row].uuid).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                        if error != nil {
                            self.myPrint(msg: error!.localizedDescription)
                            self.mySearchFriends[indexPath.row].iconContent = UIImage(named: "User")!
                            self.mySearchFriends[indexPath.row].isLoadedIcon = false
                            cell.userIcon.image = self.mySearchFriends[indexPath.row].iconContent
                            return
                        }
                        if let d = data{
                            self.mySearchFriends[indexPath.row].iconContent = UIImage.init(data: d)!
                            self.mySearchFriends[indexPath.row].isLoadedIcon = true
                            cell.userIcon.image = self.mySearchFriends[indexPath.row].iconContent
                        }
                    })
                }
                
            }else {
                cell.userIcon.image = UIImage(named: "User")!
            }
            
            cell.userName.text = self.mySearchFriends[indexPath.row].name
            cell.userHobby.text = "Age: \(Int(self.mySearchFriends[indexPath.row].age))"
            cell.userAge.text = self.mySearchFriends[indexPath.row].gender
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func updateSearchResults(for searchController: UISearchController) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !mySearching {
            self.myCurrentUser.selectedUserAge = "\(Int(self.myRecommendedFriends[indexPath.row].age))"
            self.myCurrentUser.selectedUserUUID = self.myRecommendedFriends[indexPath.row].uuid
            self.myCurrentUser.selectedUserName = self.myRecommendedFriends[indexPath.row].name
            self.myCurrentUser.selectedUserHobby = self.myRecommendedFriends[indexPath.row].hobby
            self.myCurrentUser.selectedUserGender = self.myRecommendedFriends[indexPath.row].gender
            self.myCurrentUser.selectedUserImage = self.myRecommendedFriends[indexPath.row].icon
            self.myCurrentUser.selectedUserIntro = self.myRecommendedFriends[indexPath.row].introduction
            self.myCurrentUser.selectedUserIcon = self.myRecommendedFriends[indexPath.row].iconContent
            
        }else{
            self.myCurrentUser.selectedUserAge = "\(Int(self.mySearchFriends[indexPath.row].age))"
            self.myCurrentUser.selectedUserUUID = self.mySearchFriends[indexPath.row].uuid
            self.myCurrentUser.selectedUserName = self.mySearchFriends[indexPath.row].name
            self.myCurrentUser.selectedUserHobby = self.mySearchFriends[indexPath.row].hobby
            self.myCurrentUser.selectedUserGender = self.mySearchFriends[indexPath.row].gender
            self.myCurrentUser.selectedUserImage = self.mySearchFriends[indexPath.row].icon
            self.myCurrentUser.selectedUserIntro = self.mySearchFriends[indexPath.row].introduction
            self.myCurrentUser.selectedUserIcon = self.mySearchFriends[indexPath.row].iconContent
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
            if let c = self.storyboard?.instantiateViewController(identifier: "UserInfoVC") as? UserInfoVC{
                self.navigationController?.pushViewController(c, animated: true)
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.mySearching = false
        self.myTableView.reloadData()
    }
    
    override func myRefreshTableAction() {
        if !self.mySearching {
            myKey = ["friends":self.myCurrentUser.friends,"uuid":self.myCurrentUser.uuid,"hobby":self.myCurrentUser.hobby]
            
            self.getRecommendedFriends(url: self.myNetwork.getRecommendedFriends,
                                       key: myKey!,
                                       method: .post, okMsg: "getRecommendedFriends ok",
                                       errorMsg: "getRecommendedFriends error", myReload: {
                                        
                                        self.mySearching = false
                                        self.myTableView.reloadData()
                                        self.myRefreshControl.endRefreshing()
            })
        }else{
            self.getUserFromName(url: self.myNetwork.getFriendsFromName, userName: self.mySearchName, method: .post,
                                 okMsg: "getUserFromName ok", errorMsg: "getUserFromName error", myReload: {
                                    
                                    self.mySearching = true
                                    self.myTableView.reloadData()
                                    self.myRefreshControl.endRefreshing()
            })
        }
    }
    
    func myHandleJson(json:JSON, type:String){
        let userJsonArr = json["value"]
        var userModule = [MyUserModule]()

        guard userJsonArr.count != 0 else{
            self.myRecommendedFriends = [MyUserModule]()
            self.myRefreshControl.endRefreshing()
            return
        }
        
        for index in 0...userJsonArr.count - 1{
            userModule.append(MyUserModule.init(uuid: userJsonArr[index]["uuid"].stringValue,
                                                name: userJsonArr[index]["name"].stringValue,
                                                age: userJsonArr[index]["age"].doubleValue,
                                                hobby: userJsonArr[index]["hobby"].stringValue,
                                                gender: userJsonArr[index]["gender"].stringValue,
                                                icon: userJsonArr[index]["image"].stringValue,
                                                introduction: userJsonArr[index]["introduction"].stringValue,
                                                friends: userJsonArr[index]["friends"].arrayObject as! [String],
                                                isLoadedIcon: false,
                                                iconContent: UIImage.init(named: "User")!))
        }
        
        if type == "r" {
            self.myRecommendedFriends = userModule
        }else{
            self.mySearchFriends = userModule
        }
    }
}

@available(iOS 13.0, *)
extension FindFriendVC {
    
    func getRecommendedFriends(url:String, key:[String:Any], method:HTTPMethod, okMsg:String, errorMsg:String, myReload: @escaping  ()->()){
        
        self.myAFNetworkRequest(okMessage: okMsg, errorMessage: errorMsg, url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
            
            self.mySetStartTime()
            
            DispatchQueue.main.async {
                self.myPrint(msg: json)
                self.myHandleJson(json: json, type: "r")
                
                self.mySetEndTime()
                self.myCalculateStartAndEndTime(content: "Get recommended friend --- ")
                
                myReload()
            }
        }
    }
    
    func getUserFromName(url:String, userName:String, method:HTTPMethod, okMsg:String, errorMsg:String, myReload: @escaping  ()->()){
        
        let k = ["name":userName, "uuid":self.myCurrentUser.uuid]
        
        self.myAFNetworkRequest(okMessage: okMsg, errorMessage: errorMsg, url: url, httpMethod: .post,
                                header: nil, key: k, encoding: JSONEncoding.default, mySuccess: "") { (json) in
                                    
                                    self.mySetStartTime()
                                    
                                    DispatchQueue.main.async {
                                        self.myPrint(msg: json)
                                        self.myHandleJson(json: json, type: "s")
                                        
                                        self.mySetEndTime()
                                        self.myCalculateStartAndEndTime(content: "Find friend with name --- ")
                                        
                                        myReload()
                                    }
        }
    }
}

//
//  MainVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import Alamofire
import UserNotifications

@available(iOS 13.0, *)
class MainVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTableView: UITableView!
    
    var myBlogModule = [MyBlogModule]()
    
    var myKey:[String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.myTableView.addSubview(self.myRefreshControl)
        
        myGetThisUserIcon()
        
        myKey = ["friends":self.myCurrentUser.friends]
    
        self.observeOneMyReceviceListMessage()
        self.observeOneMySentMessage()
        
        self.getBlogFromUserHobby(url: self.myNetwork.getBlogFromHobby, key: myKey!, method: HTTPMethod.post, okMsg: "homePage get blog ok", errorMsg: "homePage get blog error", myReload: {
            self.myTableView.reloadData()
            self.myRefreshControl.endRefreshing()
        })
    }
    
    func myGetThisUserIcon(){
//        if self.myCurrentUser.image != "null"{
            Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(self.myCurrentUser.uuid).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                if error != nil {
                    self.myPrint(msg: error!.localizedDescription)
                    self.myCurrentUser.userIcon = UIImage.init(named: "User")
                    return
                }
                if let d = data{
                    self.myCurrentUser.userIcon = UIImage.init(data:d)
                }
            })
//        }else{
//            self.myCurrentUser.userIcon = UIImage.init(named: "User")
//        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
                  return  self.myBlogModule.count
               }else{
                   return 0
            }
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            if self.myBlogModule.count == 0 {
                return "No post from your firned"
            }else{
                return "Your firneds' posts"
            }
        }else{
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.myBlogModule[indexPath.row].image != "null"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCellWithImage", for: indexPath) as!BlogCellWithImage
            
            self.myCircleImageView(view: [cell.userImage])
            //self.myTVStyle(textViewArr: [cell.blogContent])
            self.myCellImageView(view: [cell.blogImage], i: 10)
            
            if self.myBlogModule[indexPath.row].isLoadedImage {
                cell.blogImage.image = self.myBlogModule[indexPath.row].imageContent
            }else{
                Storage.storage().reference(forURL: self.myNetwork.fbs).child("\(myBlogModule[indexPath.row].image).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                    if error != nil {
                        self.myPrint(msg: error!.localizedDescription)
                        self.myBlogModule[indexPath.row].imageContent = UIImage(named: "User")!
                        self.myBlogModule[indexPath.row].isLoadedImage = false
                        cell.blogImage.image = self.myBlogModule[indexPath.row].imageContent
                        return
                    }
                    if let d = data{
                        self.myBlogModule[indexPath.row].imageContent = UIImage.init(data: d)!
                        self.myBlogModule[indexPath.row].isLoadedImage = true
                        cell.blogImage.image = self.myBlogModule[indexPath.row].imageContent
                    }
                })
            }
            
//            if self.myBlogModule[indexPath.row].authorImage != "null"{
                if myBlogModule[indexPath.row].isLoadedIcon {
                    cell.userImage.image = self.myBlogModule[indexPath.row].iconContent
                }else{
                    Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(myBlogModule[indexPath.row].authorUUID).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                        if error != nil {
                            self.myPrint(msg: error!.localizedDescription)
                            self.myBlogModule[indexPath.row].iconContent = UIImage(named: "User")!
                            self.myBlogModule[indexPath.row].isLoadedIcon = true
                            cell.userImage.image = self.myBlogModule[indexPath.row].iconContent
                            return
                        }
                        if let d = data{
                            self.myBlogModule[indexPath.row].iconContent = UIImage.init(data: d)!
                            self.myBlogModule[indexPath.row].isLoadedIcon = true
                            cell.userImage.image = self.myBlogModule[indexPath.row].iconContent
                        }
                    })
                }
//            }else{
//                cell.userImage.image = UIImage(named: "User")!
//            }
            cell.userName.text = self.myBlogModule[indexPath.row].authorName
            cell.blogContent.text = self.myBlogModule[indexPath.row].content
            cell.blogDate.text = self.myBlogModule[indexPath.row].dateStr
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCellWithoutImage", for: indexPath) as!BlogCellWithoutImage
            
            self.myCircleImageView(view: [cell.userImage])
            //self.myTVStyle(textViewArr: [cell.blogContent])
            
//            if myBlogModule[indexPath.row].authorImage != "null"{
                if myBlogModule[indexPath.row].isLoadedIcon {
                    cell.userImage.image = self.myBlogModule[indexPath.row].iconContent
                }else{
                    Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(myBlogModule[indexPath.row].authorUUID).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                        if error != nil {
                            self.myPrint(msg: error!.localizedDescription)
                            self.myBlogModule[indexPath.row].iconContent = UIImage(named: "User")!
                            self.myBlogModule[indexPath.row].isLoadedIcon = true
                            cell.userImage.image = self.myBlogModule[indexPath.row].iconContent
                            return
                        }
                        if let d = data{
                            self.myBlogModule[indexPath.row].iconContent = UIImage.init(data: d)!
                            self.myBlogModule[indexPath.row].isLoadedIcon = true
                            cell.userImage.image = self.myBlogModule[indexPath.row].iconContent
                        }
                    })
                }
//            }else{
//                cell.userImage.image = UIImage(named: "User")!
//            }
            cell.userName.text = self.myBlogModule[indexPath.row].authorName
            cell.blogContent.text = self.myBlogModule[indexPath.row].content
            cell.blogDate.text = self.myBlogModule[indexPath.row].dateStr
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.myBlogModule[indexPath.row].image != "null" {
            return 380 + 10 + 15
        }else{
            return 220 + 10 + 15
        }
    }
    
    @IBAction func addBlogBtn(_ sender: Any) {
        if let c = storyboard?.instantiateViewController(identifier: "AddBlogVC") as? AddBlogVC{
            navigationController?.pushViewController(c, animated: true)
        }
    }
    
    override func myRefreshTableAction(){
        self.myKey = ["friends":self.myCurrentUser.friends]
        self.getBlogFromUserHobby(url: self.myNetwork.getBlogFromHobby, key: self.myKey!, method: HTTPMethod.post, okMsg: "homePage get blog ok", errorMsg: "homePage get blog error", myReload: {
            self.myTableView.reloadData()
            self.myRefreshControl.endRefreshing()
        })
    }
    
    func getBlogFromUserHobby(url:String, key:[String:Any], method:HTTPMethod, okMsg:String, errorMsg:String, myReload: @escaping  ()->()){
        
        self.mySetStartTime()
        
        self.myAFNetworkRequest(okMessage: okMsg, errorMessage: errorMsg, url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
            
            DispatchQueue.main.async {
                self.myPrint(msg: json)
                
                let blogJsonArr = json["value"]
                var blogModule = [MyBlogModule]()
                
                guard blogJsonArr.count != 0 else{
                    self.myBlogModule = [MyBlogModule]()
                    myReload()
                    return
                }
                
                for index in 0...blogJsonArr.count - 1{
                    blogModule.append(MyBlogModule.init(uuid: blogJsonArr[index]["uuid"].stringValue,
                                                        authorUUID: blogJsonArr[index]["authorUUID"].stringValue,
                                                        image: blogJsonArr[index]["image"].stringValue,
                                                        authorImage: blogJsonArr[index]["authorImage"].stringValue,
                                                        content: blogJsonArr[index]["content"].stringValue,
                                                        date: blogJsonArr[index]["date"].doubleValue,
                                                        privacy: blogJsonArr[index]["privacy"].stringValue,
                                                        type: blogJsonArr[index]["type"].stringValue,
                                                        authorName: blogJsonArr[index]["authorName"].stringValue,
                                                        authorAge: blogJsonArr[index]["authorAge"].doubleValue,
                                                        authorHobby: blogJsonArr[index]["authorHobby"].stringValue,
                                                        authorGender: blogJsonArr[index]["authorGender"].stringValue,
                                                        dateStr: blogJsonArr[index]["dateStr"].stringValue,
                                                        isLoadedImage: false, isLoadedIcon: false))
                }
                self.myBlogModule = blogModule
                
                self.mySetEndTime()
                self.myCalculateStartAndEndTime(content: "Get blog of main page ")
                
                myReload()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (self.myCurrentUser.fromShare){
            self.myCurrentUser.fromShare = false
            
            //myCalculateStartAndEndShareTime(self.myCurrentUser.fromShareContent)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.myCurrentUser.selectedBlogUUID = self.myBlogModule[indexPath.row].uuid
        
        if let c = storyboard?.instantiateViewController(identifier: "MyCommentVC") as? MyCommentVC{
            navigationController?.pushViewController(c, animated: true)
        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.9) {
//                       self.myPresentScreenVC(withIdentifier: "MyCommentVC", type: "H")
//        }
    }
    
    func myNotifcation(title: String, subtitle:String, body:String){
          
          let content = UNMutableNotificationContent()
          content.title = title
          content.subtitle = subtitle
          content.body = body
          content.sound = UNNotificationSound.default
          
          let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
          let request = UNNotificationRequest(identifier: "notification1", content: content, trigger: trigger)
          UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
      }
    
    func observeOneMySentMessage(){
        self.ref.child("message").child("sender").child(Auth.auth().currentUser!.uid).child("list").observeSingleEvent(of: .value, with: { (snapshot) in
            
            snapshot.children.forEach { (child) in
                let snap = child as! DataSnapshot
                self.myCurrentUser.myMessageMode.mySentList.append(snap.value as! String)
            }
            
            self.ref.child("message").child("sender").child(Auth.auth().currentUser!.uid).child("list").observe(.value, with: { (snapshot) in
                
                snapshot.children.forEach { (child) in
                    let snap = child as! DataSnapshot
                    if !self.myCurrentUser.myMessageMode.mySentList.contains(snap.value as! String) {
                        self.myCurrentUser.myMessageMode.mySentList.append(snap.value as! String)
                    }
                }
                
                print("self.myCurrentUser.myMessageMode.mySentList    \(self.myCurrentUser.myMessageMode.mySentList.count)")
                self.observeMySentListMessage()
            })
        })
    }
    
    func observeMySentListMessage(){
                  
          for (index, m) in self.myCurrentUser.myMessageMode.mySentList.enumerated() {

              self.ref.child("message").child("sender")
                  .child(self.myCurrentUser.uuid).child(m).observe(.value, with: { (snapshot) in
                                          
                      for child in snapshot.children { //even though there is only 1 child
                                         let snap = child as! DataSnapshot
                                         
                                         let snapshotValue = snap.value as! [String: AnyObject]
                                                             
                                         guard let senderID = snapshotValue["senderID"] as? String else { return }
                                         guard let senderName = snapshotValue["senderName"] as? String else {return }
                                         guard let content = snapshotValue["content"] as? String else { return }
                                         
                          if (senderID != self.myCurrentUser.uuid){
                              if !self.myCurrentUser.myMessageMode.mySentMessageList.contains("\(senderID)\(senderName)\(content)") {
                                                 self.myCurrentUser.myMessageMode.mySentMessageList.append("\(senderID)\(senderName)\(content)")
                                      if self.myCurrentUser.myMessageMode.finishTheSentInit {

                                      self.myNotifcation(title: "New message", subtitle: senderName, body: content)
                                      self.myCurrentUser.myMessageMode.myCountMessageNumberOfReply.append("\(senderID)")
                              }
                          }
                      }
                  }
                      
                    if !self.myCurrentUser.myMessageMode.finishTheSentInit {
                        
                        if index == self.myCurrentUser.myMessageMode.mySentList.count - 1 {
                                self.myCurrentUser.myMessageMode.finishTheSentInit = true
                          DispatchQueue.main.async{
                            self.view.makeToast("No new message", duration: 2.0, position: .bottom)
                          }
                        }
                }
              })
         }
        
        if self.myCurrentUser.myMessageMode.mySentList.count == 0{
                self.myCurrentUser.myMessageMode.finishTheSentInit = true
                DispatchQueue.main.async{
                self.view.makeToast("No new message", duration: 2.0, position: .bottom)
            }
        }
      }
    
    func observeOneMyReceviceListMessage(){ // 先check1次看看有沒有人send比我
        self.ref.child("message").child("recipient").child(self.myCurrentUser.uuid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            snapshot.children.forEach { (child) in
                let snap = child as! DataSnapshot
                if (!self.myCurrentUser.myMessageMode.myRecipientList.contains(snap.value as! String)){
                    self.myCurrentUser.myMessageMode.myRecipientList.append(snap.value as! String)
                }
            }
                        
            self.ref.child("message").child("recipient").child(self.myCurrentUser.uuid).observe(.value, with: { (snapshot) in
                
                snapshot.children.forEach { (child) in
                    let snap = child as! DataSnapshot
                    if (!self.myCurrentUser.myMessageMode.myRecipientList.contains(snap.value as! String)){
                        self.myCurrentUser.myMessageMode.myRecipientList.append(snap.value as! String)
                    }
                }
                self.observeOneMyReceviceMessage()
            })
        })
    }
    
    func observeOneMyReceviceMessage(){ // 先check1次看看send 比我的人的訊息, 佢的uuid 加我的uuid
        
        print("self.myCurrentUser.myMessageMode.myRecipientList.count \(self.myCurrentUser.myMessageMode.myRecipientList.count)")
        
        for (index, m) in self.myCurrentUser.myMessageMode.myRecipientList.enumerated() {
            
            self.ref.child("message").child("sender").child(m).child(self.myCurrentUser.uuid).observe(.value, with: { (snapshot) in
                        
                for child in snapshot.children {
                    let snap = child as! DataSnapshot
                    
                    let snapshotValue = snap.value as! [String: AnyObject]
                                        
                    guard let senderID = snapshotValue["senderID"] as? String else { return }
                    guard let senderName = snapshotValue["senderName"] as? String else {return }
                    guard let content = snapshotValue["content"] as? String else { return }
                    
                    if (!senderID.contains(self.myCurrentUser.uuid)){
                        if !self.myCurrentUser.myMessageMode.myRecipientMessageList.contains("\(senderID)\(senderName)\(content)") {
                            self.myCurrentUser.myMessageMode.myRecipientMessageList.append("\(senderID)\(senderName)\(content)")
                            if self.myCurrentUser.myMessageMode.finishTheRecInit {
                                self.myNotifcation(title: "New message", subtitle: senderName, body: content)
                                self.myCurrentUser.myMessageMode.myCountMessageNumberOfReply.append("\(senderID)")
                            }
                    }
                }
    
                  
            }
                
                if !self.myCurrentUser.myMessageMode.finishTheRecInit{
                                      if index == self.myCurrentUser.myMessageMode.myRecipientList.count - 1 {
                                          self.myCurrentUser.myMessageMode.finishTheRecInit = true
                                      }
                                  }
         })
       }
        
        if self.myCurrentUser.myMessageMode.myRecipientList.count == 0{
            self.myCurrentUser.myMessageMode.finishTheRecInit = true
        }
    }
}

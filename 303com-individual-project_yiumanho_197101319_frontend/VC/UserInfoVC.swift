//
//  UserInfoVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON
import Alamofire

@available(iOS 11.0, *)
class UserInfoVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userGender: UILabel!
    @IBOutlet weak var userHobby: UILabel!
    @IBOutlet weak var userIntro: UITextView!
    @IBOutlet weak var userAge: UILabel!
    
    @IBOutlet weak var addFriendBtn: UIButton!
    
    var myThisUserBlog = [MyBlogModule]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userName.text = self.myCurrentUser.selectedUserName
        self.userIcon.image = self.myCurrentUser.selectedUserIcon
        self.userAge.text = "\(self.myCurrentUser.selectedUserAge)"
        self.userGender.text = self.myCurrentUser.selectedUserGender
        self.userIntro.text = self.myCurrentUser.selectedUserIntro
        self.userHobby.text = self.myCurrentUser.selectedUserHobby
        
        userIcon.layer.borderWidth = 2
        userIcon.layer.masksToBounds = false
        userIcon.layer.borderColor = UIColor.systemBlue.cgColor
        userIcon.layer.cornerRadius = userIcon.frame.height/2
        userIcon.clipsToBounds = true
        
        self.navigationItem.title = self.myCurrentUser.selectedUserName
        
        if self.myCurrentUser.friends.contains(self.myCurrentUser.selectedUserUUID){
            self.addFriendBtn.setTitle("Remove", for: .normal)
        }
        
        let myKey = ["authorUUID" : self.myCurrentUser.selectedUserUUID]
        
        self.getBlogFromThisUser(url: self.myNetwork.getBlogFromThisUser, key: myKey, okMsg: "getBlogFromThisUser ok", errorMsg: "getBlogFromThisUser error", myReload: {
            self.myTableView.reloadData()
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return self.myThisUserBlog.count
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           if self.myThisUserBlog[indexPath.row].image != "null" {
               return 380 + 50 + 15
           }else{
               return 220 + 15
           }
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
//           if myThisUserBlog[indexPath.row].image != "null"{
               let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCellWithImage", for: indexPath) as!BlogCellWithImage
               
               self.myCircleImageView(view: [cell.userImage])
               //self.myTVStyle(textViewArr: [cell.blogContent])
               self.myCellImageView(view: [cell.blogImage], i: 10)
               
               if myThisUserBlog[indexPath.row].isLoadedImage {
                   cell.blogImage.image = self.myThisUserBlog[indexPath.row].imageContent
               }else{
                Storage.storage().reference(forURL: self.myNetwork.fbs).child("\(self.myThisUserBlog[indexPath.row].image).jpg")
                       .getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                           if error != nil {
                               self.myPrint(msg: error!.localizedDescription)
                               self.myThisUserBlog[indexPath.row].imageContent = UIImage(named: "User")!
                               self.myThisUserBlog[indexPath.row].isLoadedImage = true
                               cell.blogImage.image = self.myThisUserBlog[indexPath.row].imageContent
                               return
                           }
                           if let d = data{
                               self.myThisUserBlog[indexPath.row].imageContent = UIImage.init(data: d)!
                               self.myThisUserBlog[indexPath.row].isLoadedImage = true
                               cell.blogImage.image = self.myThisUserBlog[indexPath.row].imageContent
                           }
                       })
               }
               
               cell.userName.text = self.myThisUserBlog[indexPath.row].authorName
               cell.blogContent.text = self.myThisUserBlog[indexPath.row].content
               cell.userImage.image = self.myCurrentUser.selectedUserIcon
               cell.blogDate.text = self.myThisUserBlog[indexPath.row].dateStr
               
               return cell
//           }else{
//               let cell = tableView.dequeueReusableCell(withIdentifier: "BlogCellWithoutImage", for: indexPath) as!BlogCellWithoutImage
//
//               self.myCircleImageView(view: [cell.userImage])
//               self.myTVStyle(textViewArr: [cell.blogContent])
//
//               cell.userName.text = self.myThisUserBlog[indexPath.row].authorName
//               cell.blogContent.text = self.myThisUserBlog[indexPath.row].content
//               cell.userImage.image = self.myCurrentUser.selectedUserIcon
//               cell.blogDate.text = self.myThisUserBlog[indexPath.row].dateStr
//
//               return cell
//           }
       }
    
    func getBlogFromThisUser(url:String, key:[String:Any], okMsg:String, errorMsg:String, myReload: @escaping  ()->()){
        
        self.myAFNetworkRequest(okMessage: okMsg, errorMessage: errorMsg, url: url, httpMethod: .post, header: nil, key: key, encoding: JSONEncoding.default, mySuccess: "") { (json) in
            
            DispatchQueue.main.async {
                self.myPrint(msg: json)
                
                let blogJsonArr = json["value"]
                var blogModule = [MyBlogModule]()
                
                guard blogJsonArr.count != 0 else{
                    self.myThisUserBlog = [MyBlogModule]()
                    myReload()
                    return
                }
                
                for index in 0...blogJsonArr.count - 1{
                    blogModule.append(MyBlogModule.init(uuid: blogJsonArr[index]["uuid"].stringValue,
                                                        authorUUID: blogJsonArr[index]["authorUUID"].stringValue,
                                                        image: blogJsonArr[index]["image"].stringValue,
                                                        authorImage: blogJsonArr[index]["authorImage"].stringValue,
                                                        content: blogJsonArr[index]["content"].stringValue,
                                                        date: blogJsonArr[index]["date"].doubleValue,
                                                        privacy: blogJsonArr[index]["privacy"].stringValue,
                                                        type: blogJsonArr[index]["type"].stringValue,
                                                        authorName: blogJsonArr[index]["authorName"].stringValue,
                                                        authorAge: blogJsonArr[index]["authorAge"].doubleValue,
                                                        authorHobby: blogJsonArr[index]["authorHobby"].stringValue,
                                                        authorGender: blogJsonArr[index]["authorGender"].stringValue,
                                                        dateStr: blogJsonArr[index]["dateStr"].stringValue,
                                                        isLoadedImage: false, isLoadedIcon: false))
                }
                self.myThisUserBlog = blogModule
                myReload()
            }
        }
    }
    
    @IBAction func addFriendBtn(_ sender: Any) {
        self.myPrint(msg: self.myCurrentUser.friends)
        
        let k = ["friends":self.myCurrentUser.selectedUserUUID, "uuid":self.myCurrentUser.uuid]
        
        if self.myCurrentUser.friends.contains("\(self.myCurrentUser.selectedUserUUID)") {
            
            self.myAFNetworkRequest(okMessage: "addFriendBtn remove ok", errorMessage: "addFriendBtn remove error",
                                         url: self.myNetwork.removeFriend, httpMethod: .post, header: nil, key: k,
                                         encoding: JSONEncoding.default, mySuccess: "") { (json) in
                                            
                                            DispatchQueue.main.async {
                                                
                                                if json["value"].stringValue == "successful"{
                                                    self.myPrint(msg: "Before remove \( self.myCurrentUser.friends)")
                                                    self.myCurrentUser.friends = self.myCurrentUser.friends.filter({$0 != self.myCurrentUser.selectedUserUUID})
                                                    self.displayAlert(title: "Done", message: "ok")
                                                    self.addFriendBtn.setTitle("Add", for: .normal)
                                                    self.myPrint(msg: "after remove \( self.myCurrentUser.friends)")
                                                } else{
                                                    self.myPrint(msg: "Before remove error\( self.myCurrentUser.friends)")
                                                    self.displayAlert(title: "error", message: "error")
                                                    self.myPrint(msg: "after remove error\( self.myCurrentUser.friends)")
                                                }
                                            }
            }
        }else{
            self.myAFNetworkRequest(okMessage: "addFriendBtn add ok", errorMessage: "addFriendBtn add error",
                                         url: self.myNetwork.addFrined, httpMethod: .post, header: nil, key: k,
                                         encoding: JSONEncoding.default, mySuccess: "") { (json) in
                                            
                                            DispatchQueue.main.async {
                                                
                                                if json["value"].stringValue == "successful" {
                                                    self.myPrint(msg: "Before add \( self.myCurrentUser.friends)")
                                                    self.myCurrentUser.friends.append("\(self.myCurrentUser.selectedUserUUID)")
                                                    self.myPrint(msg: "after add \( self.myCurrentUser.friends)")
                                                    self.addFriendBtn.setTitle("Remove", for: .normal)
                                                    
                                                    self.displayAlert(title: "Done", message: "ok")
                                                }else{
                                                    self.myPrint(msg: "Before add error \( self.myCurrentUser.friends)")
                                                    self.displayAlert(title: "error", message: "error")
                                                    self.myPrint(msg: "after add error\( self.myCurrentUser.friends)")
                                                }
                                            }
            }
        }
    }
    
    @IBAction func message(_ sender: Any) {
        
        self.myCurrentUser.selectedUserNameForMsg = self.myCurrentUser.selectedUserName
        self.myCurrentUser.selectedUserUUIDForMsg = self.myCurrentUser.selectedUserUUID
        
        self.myCurrentUser.channel = Channel(name: self.myCurrentUser.selectedUserNameForMsg, id: self.myCurrentUser.selectedUserUUIDForMsg)!
        
        let vc = ChatVC(u: Auth.auth().currentUser!, c: self.myCurrentUser.channel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//
//  MessageListVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase

class MessageListVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myMessageTableView: UITableView!
    
    var myMessageListArr = [MyMessageUserModule]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myMessageTableView.addSubview(myRefreshControl)
        
        self.initInRecipient()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myMessageListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        
        cell.userIcon.layer.borderWidth = 2
        cell.userIcon.layer.masksToBounds = false
        cell.userIcon.layer.borderColor = UIColor.systemBlue.cgColor
        cell.userIcon.layer.cornerRadius = cell.userIcon.frame.height/2
        cell.userIcon.clipsToBounds = true
        
        cell.messageCount.backgroundColor = .white
        
        if !myMessageListArr[indexPath.row].isLoadedName {
            self.ref.child("users").child(self.myMessageListArr[indexPath.row].uuid).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                
                let v = snapshot.value as? NSDictionary
                let name = v?["name"] as? String ?? "user"
                let iconLink = v?["image"] as? String ?? "null"
                
                self.myMessageListArr[indexPath.row].name = name
                if(iconLink == "null"){
                    self.myMessageListArr[indexPath.row].isLoadedIcon = true
                }else{
                    self.myMessageListArr[indexPath.row].isLoadedIcon = false
                }
                
                cell.userName.text = self.myMessageListArr[indexPath.row].name
                self.myMessageListArr[indexPath.row].iconLink = iconLink
                
//                if !self.myMessageListArr[indexPath.row].isLoadedIcon {
                    
                    Storage.storage().reference(forURL: self.myNetwork.fbs).child("users").child("icon").child("\(self.myMessageListArr[indexPath.row].uuid).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                        if error != nil {
                            self.myMessageListArr[indexPath.row].isLoadedIcon = true
                            self.myMessageListArr[indexPath.row].iconContent = UIImage(named: "User")!
                            self.myPrint(msg: error!.localizedDescription)
                            return
                        }
                        if let d = data{
                            self.myMessageListArr[indexPath.row].iconContent = UIImage.init(data:d)!
                            self.myMessageListArr[indexPath.row].isLoadedIcon = true
                            cell.userIcon.image = self.myMessageListArr[indexPath.row].iconContent
                        }
                    })
                    
//                }else{
//                    cell.userIcon.image = self.myMessageListArr[indexPath.row].iconContent
//                }
            }
        }else{
            cell.userName.text = self.myMessageListArr[indexPath.row].name
            
            if !myMessageListArr[indexPath.row].isLoadedIcon {
                
                Storage.storage().reference(forURL: self.myNetwork.fbs).child("\(self.myMessageListArr[indexPath.row].iconLink).jpg").getData(maxSize: Int64(self.myNetwork.size), completion: { (data, error) in
                    if error != nil {
                        self.myMessageListArr[indexPath.row].isLoadedIcon = false
                        self.myMessageListArr[indexPath.row].iconContent = UIImage(named: "User")!
                        self.myPrint(msg: error!.localizedDescription)
                        return
                    }
                    if let d = data{
                        self.myMessageListArr[indexPath.row].iconContent = UIImage.init(data:d)!
                        self.myMessageListArr[indexPath.row].isLoadedIcon = true
                        cell.userIcon.image = self.myMessageListArr[indexPath.row].iconContent
                    }
                })
                
            }else{
                cell.userIcon.image = self.myMessageListArr[indexPath.row].iconContent
            }
        }
        var myTheMessageCount = 0
        
        for newMessageCount in self.myCurrentUser.myMessageMode.myCountMessageNumberOfReply {
            if newMessageCount.contains(self.myMessageListArr[indexPath.row].uuid){
            myTheMessageCount += 1
            }
        }
        
        if myTheMessageCount > 0 {
            cell.messageCount.setTitle("\(myTheMessageCount)", for: .normal)
            cell.messageCount.backgroundColor = .systemRed
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.myCurrentUser.selectedUserNameForMsg = self.myMessageListArr[indexPath.row].name
        self.myCurrentUser.selectedUserUUIDForMsg = self.myMessageListArr[indexPath.row].uuid
        
        self.myCurrentUser.channel = Channel(name: self.myCurrentUser.selectedUserNameForMsg, id: self.myCurrentUser.selectedUserUUIDForMsg)!
        
        let cell =  tableView.cellForRow(at: indexPath) as! MessageCell
        cell.messageCount.backgroundColor = .white
        self.myCurrentUser.myMessageMode.myCountMessageNumberOfReply.removeAll{$0 == self.myMessageListArr[indexPath.row].uuid}
        
        let vc = ChatVC(u: Auth.auth().currentUser!, c: self.myCurrentUser.channel)
        //self.present(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func initInRecipient(){
        
        self.myMessageListArr.removeAll()
        
        self.ref.child("message").child("recipient").child(self.myCurrentUser.uuid).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            
            self.myShowLoadingView(displayView: self.view)
            
            snapshot.children.forEach { (child) in
                let snap = child as! DataSnapshot
                
                self.myMessageListArr.append(MyMessageUserModule.init(name: "user",
                                                                      iconLink: "null",
                                                                      isLoadedIcon: false,
                                                                      isLoadedName: false,
                                                                      uuid: snap.value as! String,
                                                                      iconContent: UIImage.init(named: "User")!))
            }
            self.initInSentList()
            
        }
    }
    
    func initInSentList(){
        self.ref.child("message").child("sender").child(self.myCurrentUser.uuid).child("list").observeSingleEvent(of: DataEventType.value) { (snapshot) in
            snapshot.children.forEach { (child) in
                let snap = child as! DataSnapshot
                
                self.myMessageListArr.append(MyMessageUserModule.init(name: "user",
                                                                      iconLink: "null",
                                                                      isLoadedIcon: false,
                                                                      isLoadedName: false,
                                                                      uuid: snap.value as! String,
                                                                      iconContent: UIImage.init(named: "User")!))
            }
            
            self.myMessageTableView.reloadData()
            self.myRemoveLoadingView()
            self.myRefreshControl.endRefreshing()
        }
    }
    
    override func myRefreshTableAction() {
        self.initInRecipient()
    }
}

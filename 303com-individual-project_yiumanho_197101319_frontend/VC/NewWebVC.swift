//
//  NewWebVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import WebKit

class NewWebVC: MyBaseVC {
    
    @IBOutlet var myWebView: WKWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let newLink = myCurrentUser.selectedNews
        let encodingNewLink = newLink.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let newURL = URL(string: encodingNewLink!)
        
        myWebView?.load(URLRequest(url: newURL!))
    }
}

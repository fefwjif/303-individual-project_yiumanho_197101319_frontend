//
//  ChatVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import MessageKit
import Firebase
import Toast_Swift

@available(iOS 11.0, *)

class ChatVC: MessagesViewController{
    
    private let user : User
    private let channel : Channel
    
    private var messages: [Message] = []
    private var messageListener: ListenerRegistration?
    
    private let db = Firestore.firestore()
    private var reference : DatabaseReference!
    
    
    private let myCurrentUser = MyCurrentUser.sharedInstance
    
    private var selectedUserRecipientList = [String]()
    private var myRecipientList = [String]()
    private var mySenderList = [String]()
    
    var myStartTime = Date()
    var myEndTime = Date()
    
    private var isFirstSend = 0
    var myLoadingView:UIView?
    
    private var myChatRecord = [Int]()
    
    private var isTheNewMessage = true
    private var isReplyMessage = false
    
    var myMessageRefHandle: DatabaseHandle!

    
    init(u : User , c: Channel) {
        self.user = u
        self.channel = c
        print("init --------- \(u.uid)")
        super.init(nibName: nil, bundle: nil)
        
        title = channel.name
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard channel.id != nil else {
            navigationController?.popViewController(animated: true)
            return
        }
        
        self.reference = Database.database().reference()
        
        navigationItem.largeTitleDisplayMode = .never
        
        maintainPositionOnKeyboardFrameChanged = true
        messageInputBar.inputTextView.tintColor = .red
        messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
        
        self.messageInputBar.delegate = self as MessageInputBarDelegate
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        self.initSelectedUserFromMyRecipientList()
        
    }
    
    private func save(messageItem: NSDictionary) {

        if self.isReplyMessage {
            self.myPrint(msg: "回覆人…")
            self.replyMessage(messageItem: messageItem)
            
        }else if self.isTheNewMessage{
            self.myPrint(msg: "新message…")
            self.sendNewMessage(messageItem: messageItem)
            
        }else if !self.isTheNewMessage{
            self.myPrint(msg: "舊message…")
            self.sendMessage(messageItem: messageItem)
            
        }
        self.messagesCollectionView.scrollToBottom()
    }
    
    // MARK: - Helpers
    
    private func insertNewMessage(_ message: Message) {
        guard !messages.contains(message) else {
            return
        }
        
        messages.append(message)
        messages.sort()
        
        let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
        let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage
        
        messagesCollectionView.reloadData()
        
        if shouldScrollToBottom {
            DispatchQueue.main.async {
                self.messagesCollectionView.scrollToBottom(animated: true)
            }
        }
    }
    
    private func handleDocumentChange(_ change: DataSnapshot) {
        guard let message = Message(dataSnapshot: change) else {
            return
        }
        
        
        insertNewMessage(message)
    }
    
    deinit {
        print("byebye")
        self.reference.child("message").removeObserver(withHandle: self.myMessageRefHandle)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.reference.child("message").removeObserver(withHandle: self.myMessageRefHandle)
    }
    
    func initSelectedUserFromMyRecipientList(){
        // 先睇我的收件箱，如有佢 ＝ 舊message （佢主） 去佢sender找
        // 再看我sender睇list有冇佢，如有佢 ＝ 舊message （我主）    去佢sender找
        // 如果收件箱都冇對方 ＝ 新
        
        self.myShowLoadingView(displayView: self.view)
        self.reference.child("message").child("recipient").child(self.myCurrentUser.uuid).observeSingleEvent(of: .value, with: { (snapshot) in // 我的
            
            snapshot.children.forEach { (child) in
                let snap = child as! DataSnapshot
                self.myRecipientList.append(snap.value as! String)
            }
            
            if self.myRecipientList.count == 0{ // 空收件，再去我sender睇
                self.isReplyMessage = false // 我不是回復者
                self.myPrint(msg: "空收件，再去我sender睇")
                self.initUserFromMySenderList()
            }else if self.myRecipientList.contains(self.myCurrentUser.selectedUserUUIDForMsg){ //有佢，我係回復者
                self.isTheNewMessage = false
                self.isReplyMessage = true
                self.myPrint(msg: "有佢，我係回復者")
                self.observeMessage() // 回者
            }
            else { // 不是空但沒佢，去我sneder睇
                self.isReplyMessage = false // 我不是回復者
                self.myPrint(msg: "不是空但沒佢，去我sneder睇")
                self.initUserFromMySenderList()
            }
        })
        
    }
    
    func initUserFromMySenderList(){
        self.reference.child("message").child("sender").child(self.myCurrentUser.uuid).child("list").observeSingleEvent(of: .value, with: { (snapshot) in
            
            snapshot.children.forEach { (child) in
                let snap = child as! DataSnapshot
                self.mySenderList.append(snap.value as! String)
            }
            
            if self.mySenderList.count == 0 { // 我發的0，是新message
                // 拎對方的收件箱，加我個名
                self.isTheNewMessage = true
                self.myPrint(msg: "我發的0，是新message")
                self.initUserFromHisOrHerRecipientList()
            }else if self.mySenderList.contains(self.myCurrentUser.selectedUserUUIDForMsg) { // 有佢，不是新message
                self.isTheNewMessage = false
                self.myPrint(msg: "有佢，不是新message")
                self.observeMessage()
            }else { // 不是空，但冇佢，新message
                self.isTheNewMessage = true
                self.myPrint(msg: "不是空，但冇佢，新message")
                // 拎對方的收件箱，加我個名
                self.initUserFromHisOrHerRecipientList()
            }
            
        })
        { (error) in
            print(error.localizedDescription)
        }
    }
    
    func initUserFromHisOrHerRecipientList(){
        self.reference.child("message").child("recipient").child(self.myCurrentUser.selectedUserUUIDForMsg).observeSingleEvent(of: .value, with: { (snapshot) in
            
            snapshot.children.forEach { (child) in
                let snap = child as! DataSnapshot
                self.selectedUserRecipientList.append(snap.value as! String)
            }
            
            self.selectedUserRecipientList.append(self.myCurrentUser.uuid) //佢的收件箱加我的名 for ready
            self.myPrint(msg: "佢的收件箱加我的名 for ready")
            self.mySenderList.append(self.myCurrentUser.selectedUserUUIDForMsg)
            self.observeMessage()
            
        })
    }
 
    func observeMessage(){
        
        var messageRef = self.reference
        var senderUUID = self.myCurrentUser.uuid
        var recipientUUID = self.myCurrentUser.selectedUserUUIDForMsg
        
        if isReplyMessage {
            self.myPrint(msg: "observe 收件箱")
            messageRef =  self.reference.child("message").child("sender").child(self.myCurrentUser.selectedUserUUIDForMsg).child(self.myCurrentUser.uuid)
            senderUUID = self.myCurrentUser.selectedUserUUIDForMsg
            recipientUUID = self.myCurrentUser.uuid
            
        }else{
            self.myPrint(msg: "observe 寄件箱")
            messageRef =  self.reference.child("message").child("sender").child(self.myCurrentUser.uuid).child(self.myCurrentUser.selectedUserUUIDForMsg)
        }
        
       self.myMessageRefHandle = messageRef!.observe(DataEventType.childAdded, with: { (s) in
                        
            self.myPrint(msg: "mon 緊")
            self.handleDocumentChange(s)
            
            self.mySetEndTime()
            self.myCalculateStartAndEndTime(content: "Send and recvice message --- ")
        })
        
        self.myRemoveLoadingView()
    }
    
    func sendNewMessage(messageItem: NSDictionary){
                
        let uniqueString = NSUUID().uuidString
        let childUpdates = ["/message/sender/\(self.myCurrentUser.uuid)/\(self.myCurrentUser.selectedUserUUIDForMsg)/\(uniqueString)": messageItem]
        let recipient = ["/message/recipient/\(self.myCurrentUser.selectedUserUUIDForMsg)/": self.selectedUserRecipientList]
        let list = ["/message/sender/\(self.myCurrentUser.uuid)/list": self.mySenderList]
        
        if !self.isTheNewMessage{
            self.myPrint(msg: "old user")
            self.reference.updateChildValues(childUpdates)
            self.mySetEndTime()
            self.myCalculateStartAndEndTime(content: "Send a message 257 --- func sendNewMessage ---")
        }else{
            if self.isFirstSend == 0{
                self.isFirstSend = 1
                myPrint(msg: "new user0")
                self.reference.updateChildValues(childUpdates) { (error, r) in
                }
                self.reference.updateChildValues(recipient)
                self.reference.updateChildValues(list)
                self.myPrint(msg: "send 左新message 了")
                
                self.mySetEndTime()
                self.myCalculateStartAndEndTime(content: "Send a message 257 --- func sendNewMessage ---")
                
            }else{
                myPrint(msg: "send 左舊message了")
                self.reference.updateChildValues(childUpdates)
                
                self.mySetEndTime()
                self.myCalculateStartAndEndTime(content: "Send a message 257 --- func sendNewMessage ---")
            }
        }
    }
    
    func sendMessage(messageItem: NSDictionary){
        
        let uniqueString = NSUUID().uuidString
        let childUpdates = ["/message/sender/\(self.myCurrentUser.uuid)/\(self.myCurrentUser.selectedUserUUIDForMsg)/\(uniqueString)": messageItem]
        
        self.reference.updateChildValues(childUpdates)
    }
    
    func replyMessage(messageItem: NSDictionary){
        
        let uniqueString = NSUUID().uuidString
        let childUpdates = ["/message/sender/\(self.myCurrentUser.selectedUserUUIDForMsg)/\(self.myCurrentUser.uuid)/\(uniqueString)": messageItem]
        
        self.reference.updateChildValues(childUpdates)
    }
    
   func myPrint(msg:Any){
        print("------------------------------------------------------")
        print(msg)
        print("")
    }
    
}

@available(iOS 11.0, *)
extension ChatVC: MessagesDisplayDelegate {
    
    internal func backgroundColor(for message: MessageType, at indexPath: IndexPath,
                                  in messagesCollectionView: MessagesCollectionView) -> UIColor {
        
        // 1
        return isFromCurrentSender(message: message) ? .primary : .incomingMessage
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath,
                             in messagesCollectionView: MessagesCollectionView) -> Bool {
        
        // 2
        return false
    }
    
    internal func messageStyle(for message: MessageType, at indexPath: IndexPath,
                               in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        
        // 3
        return .bubbleTail(corner, .curved)
    }
    
    
}

// MARK: - MessagesLayoutDelegate

@available(iOS 11.0, *)
extension ChatVC: MessagesLayoutDelegate {
    
    func avatarSize(for message: MessageType, at indexPath: IndexPath,
                    in messagesCollectionView: MessagesCollectionView) -> CGSize {
        
        // 1
        return .zero
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath,
                        in messagesCollectionView: MessagesCollectionView) -> CGSize {
        
        // 2
        return CGSize(width: 0, height: 10)
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath,
                           with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        
        // 3
        return 0
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
      avatarView.isHidden = true
    }
}


// MARK: - MessagesDataSource

@available(iOS 11.0, *)
extension ChatVC: MessagesDataSource {
    func currentSender() -> SenderType {
        return Sender(id: user.uid, displayName: MyCurrentUser.sharedInstance.name)
        
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
        
    }
    
    // 3
    func messageForItem(at indexPath: IndexPath,
                        in messagesCollectionView: MessagesCollectionView) -> MessageType {
        
        return messages[indexPath.section]
    }
    
    // 4
    func cellTopLabelAttributedText(for message: MessageType,
                                    at indexPath: IndexPath) -> NSAttributedString? {
        
        let name = message.sender.displayName
        return NSAttributedString(
            string: name,
            attributes: [
                .font: UIFont.preferredFont(forTextStyle: .caption1),
                .foregroundColor: UIColor(white: 0.3, alpha: 1)
            ]
        )
    }
}

// MARK: - UIImagePickerControllerDelegate

@available(iOS 11.0, *)
extension ChatVC: MessageInputBarDelegate {
    
    func inputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        
        self.mySetStartTime()
        
        let messageItem = ["content": text, "created": "\(Date().toString(format: .isoDateTimeMilliSec))", "senderID": self.myCurrentUser.uuid, "senderName":self.myCurrentUser.name]  as [String : Any]
        
        save(messageItem:  messageItem as NSDictionary)
        
        inputBar.inputTextView.text = ""
    }
}

@available(iOS 11.0, *)
extension ChatVC {
   func myShowLoadingView(displayView : UIView) {
    
        let mySize = CGSize(width: 150, height: 150)
        let fullScreenSize = UIScreen.main.bounds.size
            
        let loadingView = UIView.init(frame: CGRect(origin: CGPoint(x: fullScreenSize.width * 0.5, y: fullScreenSize.height * 0.5), size: mySize))
        
        loadingView.center = CGPoint(x: fullScreenSize.width * 0.5, y: fullScreenSize.height * 0.5)
        loadingView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let myActivityIndicator = UIActivityIndicatorView.init(style: .whiteLarge)
        myActivityIndicator.startAnimating()
        
        myActivityIndicator.center = CGPoint(x: loadingView.frame.width * 0.5, y: loadingView.frame.height * 0.5)
        
        DispatchQueue.main.async {
            loadingView.addSubview(myActivityIndicator)
            displayView.addSubview(loadingView)
        }
        myLoadingView = loadingView
    }
    
    func myRemoveLoadingView() {
        DispatchQueue.main.async {
            self.myLoadingView?.removeFromSuperview()
            self.myLoadingView = nil
            self.mySetEndTime()
            self.myCalculateStartAndEndTime(content: "  Init message time  437 line")
        }
    }
    
    func myCalculateStartAndEndTime(content: String) {
           
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
           let d1 = dateFormatter.date(from: dateFormatter.string(from: myStartTime))
           let d2 = dateFormatter.date(from: dateFormatter.string(from: myEndTime))

           guard d1 != nil, d2 != nil else {
               myLog(item: "start or end error")
                       return
           }

        let components = NSCalendar.current.dateComponents([.day, .hour, .minute, .second, .nanosecond], from: d1!, to: d2!)
           guard components.day != nil else {
               myLog(item: "components.day == nil")
           return
          }
           
            let myMillisecond : Int = components.toDictionary()["nanosecond"]! as! Int
            let mySecond = components.toDictionary()["second"]!
            let myM1 = Double(myMillisecond) as! Double/1000000
                  
                  myLog(item: "--------- \(content) --- second: \(components.toDictionary()["second"]!) and Millisecond:  \(myM1) --- result =  \(mySecond).\(myM1)" )
        //self.view.makeToast("--------- \(content) --- second: \(components.toDictionary()["second"]!) and Millisecond:  \(myM1) --- result =  \(mySecond).\(myM1)", duration: 3.0, position: .top)

       }
       
       func myLog(item: Any, _ file: String = #file, _ line: Int = #line, _ function: String = #function) {
           print(file + ":\(line):" + function, item)
       }
       
       func mySetStartTime(){
           self.myStartTime = Date()
       }
       
       func mySetEndTime(){
           self.myEndTime = Date()
       }
    
}


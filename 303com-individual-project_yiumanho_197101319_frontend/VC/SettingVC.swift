//
//  SettingVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingVC: MyBaseVC, UITableViewDelegate, UITableViewDataSource {

    var displayContent = ["My profile", "My friends", "My posts", "News"]
    var displayImage = ["User", "Myf", "addPost", "News"]
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        displayContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingViewCell", for: indexPath) as! SettingViewCell
        
        cell.settingLabel.text = displayContent[indexPath.row]
        cell.settingIcon.image = UIImage.init(named: displayImage[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if (indexPath.row == 0){
            if let c = storyboard?.instantiateViewController(identifier: "EditUserInfoVC") as? EditUserInfoVC{
                navigationController?.pushViewController(c, animated: true)
            }
        }else if(indexPath.row == 1){
            
            if let c = storyboard?.instantiateViewController(identifier: "MyFriendsListVC") as? MyFriendsListVC{
                navigationController?.pushViewController(c, animated: true)
            }
        }else if (indexPath.row == 2){
            
            if let c = storyboard?.instantiateViewController(identifier: "MyBlogsListVC") as? MyBlogsListVC{
                navigationController?.pushViewController(c, animated: true)
            }
        }else if indexPath.row == 3 {
            if let c = storyboard?.instantiateViewController(identifier: "NewsVC") as? NewsVC{
                navigationController?.pushViewController(c, animated: true)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Setting"
        }else{
            return ""
        }
    }
    
    @IBAction func Logout(_ sender: Any) {
        do{
            try! Auth.auth().signOut()
            DispatchQueue.main.async {
                self.myCurrentUser.userIcon = UIImage(named: "User")
                self.ref.removeAllObservers()
                self.myCurrentUser.myMessageMode.finishTheRecInit = false
                self.myCurrentUser.myMessageMode.finishTheSentInit = false
                
                self.myCurrentUser.myMessageMode.myRecipientList = [String]()
                self.myCurrentUser.myMessageMode.mySentList = [String]()
                self.myCurrentUser.myMessageMode.myRecipientMessageList = [String]()
                self.myCurrentUser.myMessageMode.mySentMessageList = [String]()
                self.myCurrentUser.myMessageMode.myCountMessageNumberOfSent = [String]()
                self.myCurrentUser.myMessageMode.myCountMessageNumberOfReply = [String]()
                
                self.myPresentScreenVC(withIdentifier: "LoginNVC", type: "F")
            }
        }
    }
}

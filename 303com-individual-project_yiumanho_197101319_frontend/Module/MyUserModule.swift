//
//  MyUserModule.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation
import UIKit

class MyUserModule {
    
    var uuid:String
    var name:String
    var age:Double
    var hobby:String
    var gender:String
    var icon:String
    var introduction:String
    var friends:[String]
    var isLoadedIcon = false
    var iconContent:UIImage?
    
    init(uuid:String, name:String, age:Double, hobby:String, gender:String, icon:String,
         introduction:String, friends:[String]) {
        self.uuid = uuid
        self.age = age
        self.hobby = hobby
        self.gender = gender
        self.icon = icon
        self.introduction = introduction
        self.friends = friends
        self.name = name
    }
    
    init(uuid:String, name:String, age:Double, hobby:String, gender:String, icon:String,
         introduction:String, friends:[String],
         isLoadedIcon:Bool, iconContent:UIImage) {
        self.uuid = uuid
        self.age = age
        self.hobby = hobby
        self.gender = gender
        self.icon = icon
        self.introduction = introduction
        self.friends = friends
        self.name = name
        self.isLoadedIcon = isLoadedIcon
        self.iconContent = iconContent
    }
}

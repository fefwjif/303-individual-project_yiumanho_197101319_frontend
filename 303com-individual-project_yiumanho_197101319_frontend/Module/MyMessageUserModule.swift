//
//  MyMessageUserModule.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation
import UIKit

class MyMessageUserModule {
    var name:String
    var iconLink:String
    var isLoadedIcon:Bool
    var isLoadedName:Bool
    var uuid:String
    var iconContent:UIImage?
    var messageContent:[String]?
    var unReadMessage = 0
    
    init(name:String, iconLink:String, isLoadedIcon:Bool, isLoadedName:Bool, uuid:String, iconContent:UIImage) {
        self.name = name
        self.iconLink = iconLink
        self.isLoadedIcon = isLoadedIcon
        self.isLoadedName = isLoadedName
        self.uuid = uuid
        self.iconContent = iconContent
    }
}

//
//  MyBlogModule.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation
import UIKit

class MyBlogModule{
    
    var uuid:String
    var authorUUID:String
    var image:String
    var authorImage:String
    var content:String
    var privacy:String
    var type:String
    var authorName:String
    var authorAge:Double
    var authorHobby:String
    var authorGender:String
    var dateStr:String
    var date:Double
    var isLoadedImage = false
    var imageContent:UIImage?
    var isLoadedIcon = false
    var iconContent:UIImage?
    
    init(uuid:String, authorUUID:String, image:String, authorImage:String, content:String, date:Double, privacy:String,
         type:String, authorName:String, authorAge:Double, authorHobby:String, authorGender:String, dateStr:String) {
        self.uuid = uuid
        self.authorUUID = authorUUID
        self.image = image
        self.authorImage = authorImage
        self.content = content
        self.date = date
        self.privacy = privacy
        self.type = type
        self.authorName = authorName
        self.authorAge = authorAge
        self.authorHobby = authorHobby
        self.authorGender = authorGender
        self.dateStr = dateStr
    }
    
    init(uuid:String, authorUUID:String, image:String, authorImage:String, content:String, date:Double, privacy:String,
            type:String, authorName:String, authorAge:Double, authorHobby:String, authorGender:String, dateStr:String,
            isLoadedImage:Bool, isLoadedIcon:Bool) {
           self.uuid = uuid
           self.authorUUID = authorUUID
           self.image = image
           self.authorImage = authorImage
           self.content = content
           self.date = date
           self.privacy = privacy
           self.type = type
           self.authorName = authorName
           self.authorAge = authorAge
           self.authorHobby = authorHobby
           self.authorGender = authorGender
           self.dateStr = dateStr
           self.isLoadedImage = isLoadedImage
           self.isLoadedIcon = isLoadedIcon
       }
}

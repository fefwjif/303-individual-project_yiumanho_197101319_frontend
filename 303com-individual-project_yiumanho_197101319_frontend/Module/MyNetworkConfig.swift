//
//  MyNetworkConfig.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Foundation

class MyNetworkCofig: NSObject {
    
    static let sharedInstance = MyNetworkCofig()
    
    var fbs:String
    var myUrl:String
    
    var size:Int64
    
    var initUser: String
    var addBlog: String
    var getBlogFromHobby: String
    var getUserFromUUID: String
    var getRecommendedFriends:String
    var getFriendsFromName:String
    var getBlogFromThisUser:String
    var addFrined:String
    var removeFriend:String
    var editUser:String
    var myFriends:String
    var myBlogs:String
    var deleteBlogs:String
    var getBlogComment:String
    var addBlogComment:String

    override init(){
        self.myUrl = "https://vtc303com-197101319.herokuapp.com"
        //"http://192.168.0.195:3000"
   //
        self.fbs = "gs://vtc303com-shape-sts.appspot.com"
        
        self.size = 1 * 102400 * 102400
        
        self.initUser = "\(self.myUrl)/users/init/"
        self.addBlog = "\(self.myUrl)/blogs/add"
        self.getBlogFromHobby = "\(self.myUrl)/blogs/getBlogFromHobby"
        self.getUserFromUUID = "\(self.myUrl)/users/fromUUID"
        self.getRecommendedFriends = "\(self.myUrl)/users/recommendedFriends"
        self.getFriendsFromName = "\(self.myUrl)/users/friendsFromName"
        self.getBlogFromThisUser = "\(self.myUrl)/blogs/blogFromThisUser"
        self.addFrined = "\(self.myUrl)/users/addFrined"
        self.removeFriend = "\(self.myUrl)/users/removeFriend"
        self.editUser = "\(self.myUrl)/users/editInfo"
        self.myFriends = "\(self.myUrl)/users/myFriends"
        self.myBlogs = "\(self.myUrl)/blogs/myBlogs"
        self.deleteBlogs = "\(self.myUrl)/blogs/deleteBlogs"
        self.addBlogComment = "\(self.myUrl)/comment/add"
        self.getBlogComment = "\(self.myUrl)/comment/getComment"
        
    }
}

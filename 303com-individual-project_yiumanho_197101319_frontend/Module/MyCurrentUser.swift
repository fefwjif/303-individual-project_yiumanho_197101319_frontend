//
//  MyCurrentUser.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Foundation

class MyCurrentUser: NSObject {
    
    var account = ""
    var name = "Yan"
    var hobby = "sports"
    var age = 20.0
    var gender = "Male"
    var uuid = "F4C29AD6-D025-4199-A9CC-E8FFF9A17DA6"
    var intro = "I am yan yan"
    var image = ""
    var friends = [String]()
    
    var myMessageMode = MyMessageMode.sharedInstance
    var myFristObserveRM = true
    var myFristObserveSM = true
    
    var selectedUserName = ""
    var selectedUserHobby = ""
    var selectedUserAge = ""
    var selectedUserGender = ""
    var selectedUserUUID = ""
    var selectedUserIntro = ""
    var selectedUserImage = ""
    var selectedUserUUIDForMsg = ""
    var selectedUserNameForMsg = ""
    var selectedNews = ""
    
    var selectedBlogUUID = ""
    
    var selectedUserIcon = UIImage(named: "user")
    var userIcon = UIImage(named: "user")
    
    var channel = Channel(name: "null")
    
    var myStartShareTime = Date()
    var myEndShareTime = Date()
    
    var fromShare = false
    var fromShareContent = ""
    
    static let sharedInstance = MyCurrentUser()
}

class MyMessageMode: NSObject{
    
    var myRecipientList = [String]()
    var mySentList = [String]()
    var myRecipientMessageList = [String]()
    var mySentMessageList = [String]()
    
    var finishTheRecInit = false
    var finishTheSentInit = false
    
    var myCountMessageNumberOfSent = [String]()
    var myCountMessageNumberOfReply = [String]()
    
    static let sharedInstance = MyMessageMode()
    
}

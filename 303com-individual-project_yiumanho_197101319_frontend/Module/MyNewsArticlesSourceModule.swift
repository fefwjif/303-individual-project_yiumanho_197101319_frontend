//
//  MyNewsArticlesSourceModule.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation

class MyNewsArticlesSourceModule{
    var id:String
    var name:String
    
    init(id: String, name:String) {
        self.id = id
        self.name = name
    }
}

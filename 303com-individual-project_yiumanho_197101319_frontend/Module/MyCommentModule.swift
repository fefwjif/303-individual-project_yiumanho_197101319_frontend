//
//  MyCommentModule.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 12/5/2020.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation
import UIKit

class MyCommentModule {
    
    var uuid:String
    var iconContent:UIImage?
    var iconLink:String
    var isLoadedIcon = false
    var commentUserName:String
    var dateStr:String
    var date:Double
    var comment:String
    var blogUUID:String
    var commenterUUID:String
    
    init(uuid:String, iconLink:String,  isLoadedIcon:Bool, commentUserName:String, dateStr:String, date:Double, comment:String, blogUUID:String, commenterUUID:String) {
        self.uuid = uuid
        self.iconLink = iconLink
        self.isLoadedIcon = isLoadedIcon
        self.commentUserName = commentUserName
        self.dateStr = dateStr
        self.date = date
        self.comment = comment
        self.blogUUID = blogUUID
        self.commenterUUID = commenterUUID
    }
}

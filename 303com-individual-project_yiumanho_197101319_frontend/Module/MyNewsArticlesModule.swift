//
//  MyNewsArticlesModule.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import Foundation
import UIKit

class MyNewsArticlesModule {
    var source:MyNewsArticlesSourceModule
    var author:String
    var title:String
    var description:String
    var url:String
    var urlToImage:String
    var publishedAt:String
    var content:String
    var isLoadedImage = false
    var imageContent:UIImage?
    
    init(source:MyNewsArticlesSourceModule, author:String, title:String,
         description:String, url:String, urlToImage:String, publishedAt:String, content:String, imageContent:UIImage?) {
        self.source = source
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
        self.imageContent = imageContent
    }
}

//
//  NewsTableViewCell.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newTitle: UILabel!
    @IBOutlet weak var newDescription: UITextView!
    @IBOutlet weak var newDate: UILabel!
    @IBOutlet weak var NewSourceName: UILabel!
    @IBOutlet weak var NewImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
//            newTitle.backgroundColor = UIColor.cyan
//                newDescription.backgroundColor = UIColor.cyan
//                newDate.backgroundColor = UIColor.cyan
//                NewSourceName.backgroundColor = UIColor.cyan
//                NewImage.backgroundColor = UIColor.cyan
    }
    
}

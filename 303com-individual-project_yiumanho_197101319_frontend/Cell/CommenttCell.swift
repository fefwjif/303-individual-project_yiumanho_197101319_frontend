//
//  CommitCell.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 12/5/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit

class CommenttCell: UITableViewCell {

    @IBOutlet weak var commentUserIcon: UIImageView!
    @IBOutlet weak var commentContent: UITextView!
    @IBOutlet weak var commentUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

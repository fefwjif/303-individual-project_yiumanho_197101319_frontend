//
//  BlogCellWithImage.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit

class BlogCellWithImage: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var blogContent: UITextView!
    @IBOutlet weak var blogImage: UIImageView!
    @IBOutlet weak var blogLike: UIButton!
    @IBOutlet weak var blogDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
//       userImage.backgroundColor = UIColor.cyan
//        userName.backgroundColor = UIColor.cyan
//        blogContent.backgroundColor = UIColor.cyan
//        blogImage.backgroundColor = UIColor.cyan
//        blogLike.backgroundColor = UIColor.cyan
//        blogDate.backgroundColor = UIColor.cyan
    }
    
}

//
//  MyBaseVC.swift
//  303com-individual-project_yiumanho_197101319_frontend
//
//  Created by A on 8/4/2020.
//  Copyright © 2020 A. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Alamofire
import SwiftyJSON
import Toast_Swift

class MyBaseVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate{
    
    var ref: DatabaseReference!
    var authStateListenerHandle: AuthStateDidChangeListenerHandle?
    var auth:Auth?
    
    var myLoadingView:UIView?
    
    var myNetwork = MyNetworkCofig.sharedInstance
    var myCurrentUser = MyCurrentUser.sharedInstance
    
    let image = UIImagePickerController()
    var mySelectedAblumImage:UIImage?
    
    var myStartTime = Date()
    var myEndTime = Date()
    
    var myRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        myRefreshControl.addTarget(self, action: #selector(self.myRefreshTableViewControl), for: UIControl.Event.valueChanged)
    }
    
    func myPrint(msg:Any){
        print("------------------------------------------------------")
        print(msg)
        print("")
    }
    
    func mySetRaiseTheView () {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(myKBShow),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(myKBHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func myRefreshTableViewControl(){
        self.myRefreshTableAction()
    }
    
    func myRefreshTableAction(){
        
    }
    
    @objc func myKBShow(notification:NSNotification){if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
        mySetKBShowAction()
        }
    }
    
    func mySetKBShowAction(){
        myKBShowAction(mySize: -200)
    }
    
    func myKBShowAction(mySize:Int){
        let width = self.view.frame.size.width;
        let height = self.view.frame.size.height;
        let rect = CGRect(origin: CGPoint(x: 0,y :mySize), size: CGSize(width: width, height: height))
        self.view.frame = rect
    }
    
    @objc func myKBHide(notification:NSNotification){
        mySetmyKBHideAction()
    }
    
    func mySetmyKBHideAction(){
        myKBHideAction(mySize: 0)
    }
    
    func myKBHideAction(mySize:Int){
        let width = self.view.frame.size.width;
        let height = self.view.frame.size.height;
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: width, height: height))
        
        self.view.frame = rect
    }
    
    func displayAlert(title:String, message:String){
        let myAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let myAction = UIAlertAction(title: "OK", style:.default , handler: nil)
        myAlert.addAction(myAction)
        self.present(myAlert,animated:true, completion: nil)
    }
    
    func myPresentScreenVC(withIdentifier:String, type:String) {
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: withIdentifier)
            if type == "F"{
                vc!.modalPresentationStyle = .fullScreen
            }
            self.present(vc!, animated: true, completion: nil)
        }
    }
    
    func myTFStyle(textFieldArr: [UITextField]){
        textFieldArr.forEach { t in
            t.layer.borderColor = UIColor.systemBlue.cgColor
            t.layer.borderWidth = 1
            t.layer.cornerRadius = 0
        }
    }
    
    func myTVStyle(textViewArr: [UITextView]){
        
        textViewArr.forEach { t in
            t.layer.borderColor = UIColor.systemBlue.cgColor
            t.layer.borderWidth = 1.0
            t.layer.cornerRadius = 0
        }
    }
    
    func myCircleImageView(view:[UIImageView]){
        
        for i in view{
            i.layer.borderWidth = 2
            i.layer.masksToBounds = false
            i.layer.borderColor = UIColor.systemBlue.cgColor
            i.layer.cornerRadius = i.frame.height/2
            i.clipsToBounds = true
        }
    }
    
    func myCellImageView(view:[UIImageView], i :Int){
        
        for v in view{
            v.layer.borderWidth = 1
            v.layer.masksToBounds = false
            v.layer.borderColor = UIColor.white.cgColor
            v.layer.cornerRadius = CGFloat(i)
            v.clipsToBounds = true
        }
    }
    
    func myShowLoadingView(displayView : UIView) {
        let mySize = CGSize(width: 150, height: 150)
        let fullScreenSize = UIScreen.main.bounds.size
        
        let loadingView = UIView.init(frame: CGRect(origin: CGPoint(x: fullScreenSize.width * 0.5, y: fullScreenSize.height * 0.5), size: mySize))
        
        loadingView.center = CGPoint(x: fullScreenSize.width * 0.5, y: fullScreenSize.height * 0.5)
        loadingView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let myActivityIndicator = UIActivityIndicatorView.init(style: .whiteLarge)
        myActivityIndicator.startAnimating()
        
        myActivityIndicator.center = CGPoint(x: loadingView.frame.width * 0.5, y: loadingView.frame.height * 0.5)
        
        DispatchQueue.main.async {
            loadingView.addSubview(myActivityIndicator)
            displayView.addSubview(loadingView)
        }
        myLoadingView = loadingView
    }
    
    func myRemoveLoadingView() {
        DispatchQueue.main.async {
            self.myLoadingView?.removeFromSuperview()
            self.myLoadingView = nil
        }
    }
    
    func myPerpareAlertAlbumAction(){
        self.image.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                self.image.sourceType = .camera
                self.present(self.image,animated:  true ,completion: nil)
            }else{
                print("Camera not available")
            }
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            self.image.sourceType = .photoLibrary
            self.present(self.image,animated:  true ,completion: nil)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet,animated: true,completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.mySelectedAblumImage? = image
            mySelectedAlbumImageTo(image: image)
        }
        
         dismiss(animated: true, completion: nil)
    }
    
    func mySelectedAlbumImageTo(image:UIImage){
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion:nil)
    }
    
    func myAFNetworkRequest(okMessage:String, errorMessage:String, url:String, httpMethod:HTTPMethod, header:HTTPHeaders?,
                            key:[String:Any], encoding:ParameterEncoding, mySuccess:String ,myCallback: @escaping (JSON) -> ()){
        Alamofire.request(url, method: httpMethod, parameters: key, encoding: encoding, headers: header).responseJSON { (r) in
            if r.result.isSuccess{
                
                self.myPrint(msg: "\(okMessage)")
                if let j:JSON = JSON(r.result.value as Any){
                    myCallback(j)
                }
                
            }else{
                print("\(errorMessage) \(String(describing: r.result.error))")
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    func myNowDateFormatter(area:String, formatter:String) -> String{
        
        let now = Date()
        
        let myDataFormatter = DateFormatter()
        myDataFormatter.locale = Locale(identifier: area)
        myDataFormatter.dateFormat = formatter
        let s = myDataFormatter.string(from: now)
        
        return s
    }
    
    func myCalculateStartAndEndShareTime(content: String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss."
        let d1 = dateFormatter.date(from: dateFormatter.string(from: self.myCurrentUser.myStartShareTime))
        let d2 = dateFormatter.date(from: dateFormatter.string(from: self.myCurrentUser.myEndShareTime))

        guard d1 != nil, d2 != nil else {
            myLog(item: "start or end error")
                    return
        }

        let components = NSCalendar.current.dateComponents([.day, .hour, .minute, .second], from: d1!, to: d2!)
        guard components.day != nil else {
            myLog(item: "components.day == nil")
        return
       }
        
        let myMillisecond : Int = components.toDictionary()["nanosecond"]! as! Int
               let mySecond = components.toDictionary()["second"]!
               let myM1 = Double(myMillisecond) as! Double/1000000
        
       myLog(item: "--------- \(content) --- second: \(components.toDictionary()["second"]!) and Millisecond:  \(myM1) --- result =  \(mySecond).\(myM1)" )
        //self.view.makeToast("--------- \(content) --- second: \(components.toDictionary()["second"]!) and Millisecond:  \(myM1) --- result =  \(mySecond).\(myM1)", duration: 3.0, position: .bottom)
    }
    
    func myCalculateStartAndEndTime(content: String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let d1 = dateFormatter.date(from: dateFormatter.string(from: myStartTime))
        let d2 = dateFormatter.date(from: dateFormatter.string(from: myEndTime))

        guard d1 != nil, d2 != nil else {
            myLog(item: "start or end error")
                    return
        }

        let components = NSCalendar.current.dateComponents([.day, .hour, .minute, .second, .nanosecond], from: d1!, to: d2!)
        guard components.day != nil else {
            myLog(item: "components.day == nil")
        return
       }
        
        let myMillisecond : Int = components.toDictionary()["nanosecond"]! as! Int
        let mySecond = components.toDictionary()["second"]!
        let myM1 = Double(myMillisecond) as! Double/1000000
        
        myLog(item: "--------- \(content) --- second: \(components.toDictionary()["second"]!) and Millisecond:  \(myM1) --- result =  \(mySecond).\(myM1)" )
        //self.view.makeToast("--------- \(content) --- second: \(components.toDictionary()["second"]!) and Millisecond:  \(myM1) --- result =  \(mySecond).\(myM1)", duration: 3.0, position: .bottom)
    }
    
    func myToast(content:String){
        self.view.makeToast(content, duration: 3.0, position: .bottom)
    }
    
    func myLog(item: Any, _ file: String = #file, _ line: Int = #line, _ function: String = #function) {
        print(file + ":\(line):" + function, item)
    }
    
    func mySetStartTime(){
        self.myStartTime = Date()
    }
    
    func mySetEndTime(){
        self.myEndTime = Date()
    }
}
